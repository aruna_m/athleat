# description about module
{
	'name': 'Athleat',
	'version': '1.0',
	'category': 'athleat', 

	'description': """
	Athleat module
	""",

	'author': 'Anipr',
	'website': 'www.anipr.in',
	'depends': ['base','mail','email_template',
				'web_widget_datepicker_options',
				'board',
				'web_readonly_bypass',
				'auth_signup',
	],
	
	'data': [
	
			'security/mealplans_security_view.xml',
			'security/ir.model.access.csv',

			'views/sequences.xml',
			'views/schedulers.xml',
			
			'edi/mail_data.xml',
			
			'views/dashboard_customers.xml',
			'views/dashboard_meal_plans.xml',
			'views/dashboard_meals_view.xml',
			'views/dashboard_plan_analysis.xml',

			'wizard/display_meals_view.xml',
			'wizard/kitchen_view.xml',
			'wizard/meal_wizard_view.xml',
			'wizard/daywise_meals_view.xml',
			'wizard/mealplans_import_view.xml',
			
			'views/meal_plans_view.xml',
			'views/customers_view.xml',
			'views/ingredients_view.xml',
			'views/meal_items_view.xml',
			'views/addons_view.xml',
			'views/sauce_view.xml',
			'views/weekly_plans_view.xml',
			'views/likes_dislikes_view.xml', 
			'views/master_plans_view.xml',
			'views/meal_options_view.xml',
			'views/exclusion_price_view.xml',
			'views/customer_payments_view.xml',
			'views/request_payments_view.xml',
			
			'reports/report_mealplans_view.xml',
			'reports/report_mealplans_template.xml',
			'reports/report_mealitems_view.xml',
			'reports/report_mealitems_template.xml',
			'reports/report_plan_analysis_template.xml',
			'reports/report_plan_analysis_view.xml',
			'reports/display_meals_view.xml',
			'reports/display_meals_template.xml',
			'reports/customer_meals_report_view.xml',
			'reports/customer_meals_report_view_template.xml',
			'reports/daywise_meal_report.xml',
			'reports/daywise_meal_report_template.xml',

			'reports/kitchen_report.xml',
# 

			
			

	],

	# 'demo': [],
	'installable': True,
	'auto_install': False,
	'application': True,
}
