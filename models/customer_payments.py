from openerp.osv import osv,fields
from datetime import timedelta,datetime
import time
from openerp.tools.translate import _

from openerp.exceptions import except_orm, Warning, RedirectWarning

class Customer_payments(osv.osv):
	
	_name = "customer.payments"	

	_rec_name = "customer_id"

	_columns = {
	'customer_id':fields.many2one('res.partner','Customer'),
	'plan_id':fields.many2one('meal.plans','Meal Plan', ondelete='cascade'),
	'payment_mode': fields.selection([('cod','Cash On Delivery'),
		('online','Online')],'Payment Mode'),
	'reference':fields.char('Reference'),
	'merchant_reference':fields.char('Merchant Reference'),
	'date_time':fields.datetime('Date Time'),
	'amount':fields.float('Amount'),
	'status':fields.selection([('unpaid','Unpaid'),
							('paid','Paid'),],'Status'),
	'address': fields.text('Address to collect'),
	'collect_time': fields.datetime('Date & Time to collect Cash'),
	'description': fields.text('Description'),

	'email':fields.char('Email'),
	'signature':fields.char('Signature'),
	'fort_id':fields.char('Fort ID'),
	'response_message':fields.text('Response Message'),
	}
	_defaults = {

		'date_time': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
		'status': 'unpaid',
	}	

	
	def unlink(self, cr, uid, ids, context=None):
		for obj in self.browse(cr, uid,ids, context):
			if obj.status in ('paid'):
				raise Warning(_('Warning!!'), _('You cannot delete a Customer Payment which is already in Paid state!!'))
		return super(Customer_payments, self).unlink(cr, uid, ids, context)

	def paid_status(self,cr,uid,ids,context=None):
		return self.write(cr,uid,ids,{'status':'paid'})
Customer_payments()
