from openerp.osv import osv,fields
from openerp import api, SUPERUSER_ID
import random
from datetime import datetime, timedelta, date
import time
import HTML
from openerp.exceptions import except_orm, Warning

from openerp.tools.translate import _

class Meals(osv.osv):

	_name="meal.meal"

	def _get_price_per_day(self, cr, uid, ids, field_name, arg, context=None):

		res = {}
		for obj in self.browse(cr, uid, ids):
			price = 0
			for item in obj.plan_meal_items:
				price += item.price
			price += obj.extra_sauce.price

			res[obj.id] = price

		return res


	def _get_customized_items(self, cr, uid, ids, field_name, arg, context=None):

		res = {}
		for obj in self.browse(cr, uid, ids):
			items = ''
			for item in obj.plan_meal_items:
				if item.grams and item.item_id.name:
					items += item.item_id.name + '('+str(item.grams)+')'
				items += ', '

			res[obj.id] = items

		return res


	def _get_items_names(self, cr, uid, ids, field_name, arg, context=None):
		res = {}
		
	
		for obj in self.browse(cr, uid, ids):
			items = ''
			if obj.carb_type in ['low carb','high carb']:
				if obj.item1: items += obj.item1.name + '(' + str(obj.item1.quantity) + ')' + '(Breakfast), \n' 
				if obj.item2: items += obj.item2.name + '(' + str(obj.item2.quantity) + ')' + '(MID), \n'
				if obj.item3: items += obj.item3.name + '(' + str(obj.item3.quantity) + ')' + '(Evening), \n'
				if obj.item4: items += obj.item4.name + '(' + str(obj.item4.quantity) + ')' + '(Night) '
			elif obj.carb_type == 'customize':
				if obj.meal11 : items += obj.meal11+', '
				if obj.meal12 : items += obj.meal12+', '
				if obj.meal13 : items += obj.meal13+', '
				if obj.sauce1 : items += obj.sauce1+',  \n'
				if obj.meal21 : items += obj.meal21+', '
				if obj.meal22 : items += obj.meal22+', '
				if obj.meal23 : items += obj.meal23+', '
				if obj.sauce2 : items += obj.sauce2+', \n'
				if obj.meal31 : items += obj.meal31+', '
				if obj.meal32 : items += obj.meal32+', '
				if obj.meal33 : items += obj.meal33+', '
				if obj.sauce3 : items += obj.sauce3+', \n'
				if obj.meal41 : items += obj.meal41+', '
				if obj.meal42 : items += obj.meal42+', '
				if obj.meal43 : items += obj.meal43+', '
				if obj.sauce3 : items += obj.sauce3
			else:
				if obj.item1: items += obj.item1.name + '(' + str(obj.item1.quantity) + ')' + '(Breakfast), \n' 
				if obj.item2: items += obj.item2.name + '(' + str(obj.item2.quantity) + ')' + '(MID), \n'
				if obj.item3: items += obj.item3.name + '(' + str(obj.item3.quantity) + ')' + '(Evening), \n'
				if obj.item4: items += obj.item4.name + '(' + str(obj.item4.quantity) + ')' + '(Night) '
			res[obj.id] = items

		return res
	
	_columns={

	'cust':fields.many2one('res.partner','Customer', domain="[('customer','=',True)]"),
	'status': fields.selection([
								('scheduled','Scheduled'),
								('undelivered','Undelivered'),
								('served','Delivered'),],'Status'),
	'week1_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week2_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week3_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week4_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week5_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week6_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week7_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week8_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week9_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week10_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week11_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	'week12_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	
	'plan_rel_id':fields.many2one('meal.plans','Relation to Meal plan', ondelete='cascade'),
	
	'master_week1id':fields.many2one('master.plans','Relation to Master plans', ondelete='cascade'),
	'master_week2id':fields.many2one('master.plans','Relation to Master plans', ondelete='cascade'),
	'master_week3id':fields.many2one('master.plans','Relation to Master plans', ondelete='cascade'),
	'master_week4id':fields.many2one('master.plans','Relation to Master plans', ondelete='cascade'),

	'cust_master_week1id':fields.many2one('master.plans','Relation to Customised Master plans', ondelete='cascade'),
	'cust_master_week2id':fields.many2one('master.plans','Relation to Customised Master plans', ondelete='cascade'),
	'cust_master_week3id':fields.many2one('master.plans','Relation to Customised Master plans', ondelete='cascade'),
	'cust_master_week4id':fields.many2one('master.plans','Relation to Customised Master plans', ondelete='cascade'),

	'master_history_id':fields.many2one('master.plans','Relation to Master History plans', ondelete='cascade'),
	'weekly_id1':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id2':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id3':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id4':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id5':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id6':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id7':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id8':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id9':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id10':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id11':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),
	'weekly_id12':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),

	'weekid':fields.many2one('weekly.plans','Relation to Weekly plan', ondelete='cascade'),

	'plan_analysis_id':fields.many2one('plan.analysis','Relation to plan analysis'),
	'res_partner1_id':fields.many2one('res.partner','Relation to res.partner'),
	'res_partner2_id':fields.many2one('res.partner','Relation to res.partner'),
	'carb_type':fields.selection([('high carb','High Carb'),
								('low carb','Low Carb'),
								('customize','Customized')],'Carb Type'),
	'meal_preference':fields.selection([('chick','Chicken'),
										('beef','Beef'),
										('fish','Fish'),
										('lamb','Lamb'),
										('veg','Veg')],'Meal Category'),

	'quantity':fields.integer('Grams'),
	'price':fields.float('Price'),
	'carb':fields.float('Carb'),
	'protein':fields.float('Protein'),
	'fat':fields.float('Fat'),
	'calories':fields.float('Calories'),
	'meal_plan_items_id':fields.many2many('recipies.meal','mealplans_recipies_rel','item','rel_id','Items'),
	'scheduled_daily_items_id':fields.many2many('recipies.meal','scheduled_daily_recipies_rel','item','rel_id','Items'),

	'ingredients':fields.many2many('ingredient.menu','rel_meals','meals_rel_id','rel_id','Ingredients'),
	'addons':fields.many2many('addons.conf','meals_addons_relid','meals_id','addons_rel_id','Addons'),
	'date':fields.date('Date'),

	"meals_type":fields.selection([('regular','Regular'),
									('subsitution','Subsitution')],'Meals Type'),

	'week':fields.char('Week'),
	'meal_name':fields.char('Meal Name'),
	'meals_per_day':fields.integer("Meals to be Cooked"),
	'day': fields.selection([(1,'Day-1'),
							(2,'Day-2'),
							(3,'Day-3'),
							(4,'Day-4'),
							(5,'Day-5')],'Day'),
	
	'plan_meal_items':fields.one2many('meal.plan.items','meal_id',string='Items'),
	'total_price_day': fields.function(_get_price_per_day, type="float", string="Price/Day"),
	'customized_items': fields.function(_get_customized_items, type="char", string="Items"),
	'customized_scheduled_meals': fields.function(_get_items_names, type="char", string="Items"),



	'item1':fields.many2one('recipies.meal','Items'),
	'item2':fields.many2one('recipies.meal','Items'),
	'item3':fields.many2one('recipies.meal','Items'),
	'item4':fields.many2one('recipies.meal','Items'),

	'meal11':fields.char('Protein'),
	'meal12':fields.char('Protein'),
	'meal13':fields.char('Protein'),
	'sauce1':fields.char('Protein'),

	'meal21':fields.char('Veg'),
	'meal22':fields.char('Veg'),
	'meal23':fields.char('Veg'),
	'sauce2':fields.char('Veg'),

	'meal31':fields.char('Sauce'),
	'meal32':fields.char('Sauce'),
	'meal33':fields.char('Sauce'),
	'sauce3':fields.char('Sauce'),

	'meal41':fields.char('Carb'),
	'meal42':fields.char('Carb'),
	'meal43':fields.char('Carb'),
	'sauce4':fields.char('Carb'),

	'cusitem1':fields.char('Custitem1'),
	'cusitem2':fields.char('Custitem2'),
	'cusitem3':fields.char('Custitem3'),
	'cusitem4':fields.char('Custitem4'),
	
	'spl_req':fields.text('Special Request'),
	'count':fields.integer('Count'),

	'liked_meals2_ids':fields.many2many('likes.dislikes','rel_liked2_meals','rel2_id','recipie2_id','Liked Meals'),
	'disliked_meals2_ids':fields.many2many('likes.dislikes','rel_disiked2_meals','rel2_id','recipie2_id','Disliked Meals/Allergies'),

	'duplicated_date':fields.datetime('Duplicated Date'),
	'replaced_date':fields.datetime('Replaced Date'),
	'meal_no': fields.selection([(1,'Meal-1'),
								(2,'Meal-2'),
								(3,'Meal-3'),
								(4,'Meal-4')],'Meal No.'),
	'extra_sauce':fields.many2one('sauce.menu','Sauce'),
	# 'customized_scheduled_meals':fields.char(string="Items"),
	}
	
	@api.onchange('item','quantity')
	def _onchange_amount(self):
		if self.item:
			self.carb = self.item.carb
			self.protein = self.item.protein
			self.fat = self.item.fat
			self.price = self.item.price
			self.calories = self.item.calories
		if self.quantity:
			if self.quantity >100:
				grams_no = (self.quantity)/100
				self.carb = self.item.carb * grams_no
				self.protein = self.item.protein * grams_no
				self.fat = self.item.fat * grams_no
				self.price = self.item.price * grams_no
				self.quantity = self.item.quantity * grams_no
				self.calories = self.item.calories * grams_no
			else:
				pass

	@api.onchange('items')
	def _onchange_amount(self):
		if self.items:

			self.quantity =self.calories=self.price =self.fat=self.protein =self.carb=0
			for i in self.items:
				self.quantity =self.quantity + i.quantity
				self.calories =self.calories + i.calories
				self.price =self.price + i.price
				self.fat =self.fat + i.fat
				self.protein =self.protein + i.protein
				self.carb =self.carb + i.carb
		else:
			pass


	def write(self, cr, uid, ids, vals, context=None):

		replacing = vals.get('replaced', None)
		duplicating = vals.get('duplicated', None)

		if replacing:
			for i in self.browse(cr, uid, ids):
				if i.replaced_date:
					replaced_date = datetime.strptime(str(i.replaced_date), "%Y-%m-%d %H:%M:%S")
					limit_date = replaced_date + timedelta(days=1)
					if datetime.now() <=  limit_date:
						raise osv.except_osv(_('Error!'),_('You can not replace a meal twice in a day!!'))

			vals.update({'replaced_date': datetime.now()})

		if duplicating:
			for i in self.browse(cr, uid, ids):
				if i.duplicated_date:
					duplicated_date = datetime.strptime(str(i.duplicated_date), "%Y-%m-%d %H:%M:%S")
					limit_date = duplicated_date + timedelta(days=1)
					if datetime.now() <=  limit_date:
						raise osv.except_osv(_('Error!'),_('You can not duplicate a meal twice in a day!!'))
			vals.update({'duplicated_date': datetime.now()})

		return super(Meals, self).write(cr, uid, ids, vals, context=context)


	def undelivered_action(self,cr,uid,ids,context=None):
		return self.write(cr,uid,ids,{'status':'undelivered'},context=context)
	

	def confirming_status(self,cr,uid,context=None):

		meals_list = self.search(cr, uid, [('status', '=', 'scheduled')], context=context)
		for each in meals_list:
			if self.browse(cr,uid,each).date:
				if datetime.today() > datetime.strptime(self.browse(cr,uid,each).date, '%Y-%m-%d'):
					self.write(cr,uid,each, {'status': 'served'})
				else:
					pass
		return True
	
	def send_daily_meals_admin(self,cr,uid,context=None):
		template = self.pool.get('ir.model.data').get_object(cr, uid, 'athleat', 'notify_meals_admin')
		values={}
		meal_itms =''
		# admin_email = str(self.pool.get('res.users').browse(cr,uid,SUPERUSER_ID).email)

		admin_mail_list =[]
		admin_list = self.pool.get('res.users').search(cr, uid, [('groups_id.name','=','Superadmin'), ('groups_id.category_id.name','=','Meal Plans Access Levels')])
		for i in admin_list:
			admin_mail_list += [str(self.pool.get('res.users').browse(cr,uid,i).email)]
	
		table_data = HTML.Table(header_row=['Name','Carb Type','Date', 'Meal Items'])

		tomorrow = datetime.strftime(datetime.now().date() + timedelta(days=1), '%Y-%m-%d')
		scheduled_meals = self.search(cr,uid,[('date','=',tomorrow)])
		
		if scheduled_meals:
			for i in scheduled_meals:
				meal_itms = ''
				rec = self.browse(cr,uid,i)
				
				date, name, carb_type = rec.date, rec.cust.name, rec.carb_type
				item1, item2, item3, item4 = rec.item1.name, rec.item2.name, rec.item3.name, rec.item4.name
				
				if item1: meal_itms += str(item1) + '(Breakfast), '
				if item2: meal_itms += str(item2) + '(Mid), '
				if item3: meal_itms += str(item3) + '(Evening), '
				if item4: meal_itms += str(item4) + '(Night), '

				table_data.rows.append([str(name), str(carb_type), str(date), str(meal_itms)])

			if admin_mail_list:
				for y in admin_mail_list:
					email_template_obj = self.pool.get('email.template')
					values = email_template_obj.generate_email_batch(cr, uid, template.id, [uid], context=context)
					values['subject'] = 'Customer Meal Details' 
					values['email_to'] = y
					values['email_from'] = 'hello@athleat.ae'
					values['body_html'] = "Dear Admin,<div><br></div><div>Following are the details of planned meals for tomorrow :</div><div>%s\n</div><div><br></div><div>Regards,</div><div>Athleat Team.</div>"%table_data
					values['body'] = 'body_html',
					values['res_id'] = False
					mail_mail_obj = self.pool.get('mail.mail')
					x = uid
					values[str(x)] = values.pop(uid)
					msg_id = mail_mail_obj.create(cr,uid,values)
					mail_mail_obj.send(cr, uid, [msg_id], context=context)
			
		return True
	
Meals()

class History_Meal(osv.osv):

	_name="history.meal"
	
	_columns={ 


		'day':fields.char('Day'),

		'quantity':fields.integer('Grams'),
		'price':fields.float('Price'),
		'carb':fields.float('Carb'),
		'protein':fields.float('Protein'),
		'fat':fields.float('Fat'),
		'calories':fields.float('Calories'),
		'meal_plan_type': fields.selection([('high carb','High Carb'),
								('low carb','Low Carb'),
								('customize','Customized')],'Carb Type'),

		'rels_id':fields.many2one('meal.plans','Relation to Meal plan'),
		"meals_type":fields.selection([('regular','Regular'),
									('subsitution','Subsitution')],'Meals Type'),

		'items':fields.many2many('recipies.meal','history_items_id','item','hist_rel_id','Items'),

		'addons':fields.many2many('addons.conf','hist_addons_id','adds_id','hist_meals_id','Addons'),

		}


class meal_plan_items(osv.osv):

	_name = 'meal.plan.items'

	# _rec_name= 'item_id'

	_columns = {
	'cust':fields.many2one('res.partner','Customer'),
	'meal_id': fields.many2one('meal.meal', 'Plan ID', ondelete='cascade'),
	'item_id': fields.many2one('recipies.meal', 'Item', ondelete='cascade'),
	'grams': fields.selection([
		(1,'0'),
		(100,'100'),
		(150,'150'),
		(200,'200'),
		(250,'250'),
		(300,'300')],'Grams'),
	'meal_no': fields.selection([(1,'Meal-1'),
		(2,'Meal-2'),
		(3,'Meal-3'),
		(4,'Meal-4')],'Meal No.'),
	'protein': fields.float('Protein'),
	'carb': fields.float('Carb'),

	'fat': fields.float('Fat'),
	'price': fields.float('Price'),
	'meal_cat':fields.selection([('protein','Protein'),('carb','Carbohydrates'),('veg','Vegetables')],'Category'),
	'extra_sauce':fields.many2one('sauce.menu','Sauce'),
	}

	_defaults = {
	'grams': 100,
	}

	@api.onchange('item_id','grams')
	def _onchange_quantity(self):
		if self.item_id:
			if self.item_id.quantity == 0:
				grams_no = 1
			else:
				grams_no = (self.grams)/float(self.item_id.quantity)
			self.carb = self.item_id.carb * grams_no
			self.protein = self.item_id.protein * grams_no
			self.fat = self.item_id.fat * grams_no
			self.price = self.item_id.price * grams_no
