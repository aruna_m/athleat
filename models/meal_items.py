from openerp.osv import osv,fields
from openerp import tools,api
# import xlrd
# from xlrd import open_workbook
import os

class Recipies(osv.osv):

	_name ="recipies.meal"	
	
	_rec_name ="name"
	
	def _get_image(self, cr, uid, ids, name, args, context=None):
		result = dict.fromkeys(ids, False)
		for obj in self.browse(cr, uid, ids, context=context):
			result[obj.id] = tools.image_get_resized_images(obj.image, avoid_resize_medium=True)
		return result

	def _set_image(self, cr, uid, id, name, value, args, context=None):
		return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

	_columns={
	
	
	'name':fields.char('Name'),
	'recipie_id':fields.many2one('res.partner','Type'),
	'meal_preference':fields.selection([
										# ('breakfast','Breakfast'),
										('chick','Chicken'),
										('meat','Meat'),
										('fish','Fish'),
										# ('lamb','Lamb'),
										('veg','Veg')],'Meal Category'),
	'carb':fields.float('Carb'),
	'quantity':fields.integer('Grams'),
	'calories':fields.float('Calories'),
	'protein':fields.float('Protein'),
	'fat':fields.float('Fat'),
	'price':fields.float('Unit Price'),
	'rel_id':fields.many2one('meal.meal','Relation'),
	'hist_rel_id':fields.many2one('history.meal','Relation'),
	'sauce_id':fields.many2one('sauce.menu','Sauce'),
	# 'weekly_plan_ids':fields.many2many('meal.meal','weekly_recipies_rel','rel_id','item','Recipies'),

	'ingredients':fields.many2many('ingredient.menu','rel_recipies','meals_rel_id','rel_id','Ingredients'),
	'image': fields.binary("Image",
			help="This field holds the image used as image for the product, limited to 1024x1024px."),
		'image_medium': fields.function(_get_image, fnct_inv=_set_image,
			string="Medium-sized image", type="binary", multi="_get_image", 
			store={
				'recipies.meal': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
			},
			help="Medium-sized image of the product. It is automatically "\
				 "resized as a 128x128px image, with aspect ratio preserved, "\
				 "only when the image exceeds one of those sizes. Use this field in form views or some kanban views."),
		'image_small': fields.function(_get_image, fnct_inv=_set_image,
			string="Small-sized image", type="binary", multi="_get_image",
			store={
				'recipies.meal': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
			},
			help="Small-sized image of the product. It is automatically "\
				 "resized as a 64x64px image, with aspect ratio preserved. "\
				 "Use this field anywhere a small image is required."),
	"meals_type":fields.selection([('regular','Regular'),
									('subsitution','substitution')],'Meals Type'),
	'carb_type':fields.selection([('high carb','High Carb'),
								('low carb','Low Carb'),
								('customize','Customized'),
								],'Carb Type'),	

	'req_id':fields.many2one('likes.dislikes.requests','Request ID'),
	'category': fields.selection([('protein','Protein'),
		('carb','Carbs'),
		('fat','Fat'),
		('veg','Veg')],'Category'),

	'customize_category': fields.selection([('gourmet','Gourmet Recipe/Pre Built'),
		('own','Create Your Own')],'Prebuilt/Create Own'),
	'analysis_id':fields.many2one('plan.analysis','Analysis'),
	'breakfast':fields.boolean('Breakfast'),
	'random_count':fields.integer('Random Count'),
	
	}
	
	def import_meal_items(self,cr,uid,ids,context=None):
		

		vals={}
		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		xl_workbook = xlrd.open_workbook(BASE_DIR +'/excel/Athleat Meals with Macros and Ingredients (X).xlsx')#opening a xsl file
		sheet_names = xl_workbook.sheet_names()#retrieving all sheet names in that xsl file
		
		xl_sheet0 = xl_workbook.sheet_by_name(sheet_names[0])#accessing the first sheet of workbook
		for row in range(3,xl_sheet0.nrows):

			name = xl_sheet0.cell(row, 1).value
			carbs=xl_sheet0.cell(row, 5).value
			protein=xl_sheet0.cell(row, 6).value
			fat=xl_sheet0.cell(row, 7).value
			sauce=xl_sheet0.cell(row, 3).value
			if sauce:
				sauce_id = self.pool.get('sauce.menu').search(cr, uid, [('name', 'like', str(sauce))]) 
				if sauce_id:
					sauce_id = sauce_id[0]
				else:
					sauce_id = self.pool.get('sauce.menu').create(cr, uid, {'name': str(sauce)})
			ingg=xl_sheet0.cell(row, 9).value.split(',')
			ing_idd=[]
			for i in ingg:
				if i:
					ing_id = self.pool.get('ingredient.menu').search(cr, uid, [('ingredient_name', 'like', str(i))]) 
				if ing_id:
					ing_id = ing_id[0]
				else:
					ing_id = self.pool.get('ingredient.menu').create(cr, uid, {'ingredient_name': str(i)})
				ing_idd+=[ing_id]

			if row<13:
				carb_type='low carb'
			else:
				carb_type='high carb'

			vals={
				'name':name,
				'meal_preference':'chick',
				'carb':carbs,
				'protein':protein,
				'fat':fat,
				'ingredients':[(6,0,ing_idd)],
				'carb_type':carb_type,
				'sauce_id':sauce_id,
			}
			self.pool.get('recipies.meal').create(cr,uid,vals,context=context)

		xl_sheet1 = xl_workbook.sheet_by_name(sheet_names[1])#accessing the second sheet of workbook
		for row in range(3,xl_sheet1.nrows):
			name = xl_sheet1.cell(row, 1).value
			carbs=xl_sheet1.cell(row, 4).value
			protein=xl_sheet1.cell(row, 5).value
			fat=xl_sheet1.cell(row, 6).value
			sauce=xl_sheet1.cell(row, 3).value
			if sauce:
				sauce_id = self.pool.get('sauce.menu').search(cr, uid, [('name', 'like', str(sauce))]) 
				if sauce_id:
					sauce_id = sauce_id[0]
				else:
					sauce_id = self.pool.get('sauce.menu').create(cr, uid, {'name': str(sauce)})
			if row<12:
				carb_type='low carb'
			else:
				carb_type='high carb'

			ingg=xl_sheet1.cell(row, 8).value.split(',')
			ing_idd=[]
			for i in ingg:
				if i:
					ing_id = self.pool.get('ingredient.menu').search(cr, uid, [('ingredient_name', 'like', str(i))]) 
				if ing_id:
					ing_id = ing_id[0]
				else:
					ing_id = self.pool.get('ingredient.menu').create(cr, uid, {'ingredient_name': str(i)})
				ing_idd+=[ing_id]
			vals={
				'name':name,
				'meal_preference':'beef',
				'carb':carbs,
				'protein':protein,
				'fat':fat,
				'ingredients':[(6,0,ing_idd)],
				'carb_type':carb_type,
				'sauce_id':sauce_id,			}
			self.pool.get('recipies.meal').create(cr,uid,vals,context=context)

		xl_sheet2 = xl_workbook.sheet_by_name(sheet_names[2])#accessing the second sheet of workbook
		for row in range(3,xl_sheet2.nrows):
			name = xl_sheet2.cell(row, 1).value
			carbs=xl_sheet2.cell(row, 6).value
			protein=xl_sheet2.cell(row,7).value
			fat=xl_sheet2.cell(row, 8).value
			sauce=xl_sheet2.cell(row, 4).value
			if sauce:
				sauce_id = self.pool.get('sauce.menu').search(cr, uid, [('name', 'like', str(sauce))]) 
				if sauce_id:
					sauce_id = sauce_id[0]
				else:
					sauce_id = self.pool.get('sauce.menu').create(cr, uid, {'name': str(sauce)})
			if row<9:
				carb_type='low carb'
			else:
				carb_type='high carb'

			ingg=xl_sheet2.cell(row, 10).value.split(',')
			ing_idd=[]
			for i in ingg:

				if i:
					ing_id = self.pool.get('ingredient.menu').search(cr, uid, [('ingredient_name', 'like', str(i))]) 
				if ing_id:
					ing_id = ing_id[0]
				else:
					ing_id = self.pool.get('ingredient.menu').create(cr, uid, {'ingredient_name': str(i)})
				ing_idd+=[ing_id]

			vals={
				'name':name,
				'meal_preference':'lamb',
				'carb':carbs,
				'protein':protein,
				'fat':fat,
				'sauce_id':sauce_id,
				'ingredients':[(6,0,ing_idd)],
				'carb_type':carb_type,
			}
			self.pool.get('recipies.meal').create(cr,uid,vals,context=context)

		xl_sheet3 = xl_workbook.sheet_by_name(sheet_names[3])#accessing the second sheet of workbook
		for row in range(3,xl_sheet3.nrows):
			name = xl_sheet3.cell(row, 1).value
			carbs=xl_sheet3.cell(row, 5).value
			protein=xl_sheet3.cell(row, 6).value
			fat=xl_sheet3.cell(row, 7).value
			sauce=xl_sheet3.cell(row, 3).value
			if sauce:
				sauce_id = self.pool.get('sauce.menu').search(cr, uid, [('name', 'like', str(sauce))]) 
				if sauce_id:
					sauce_id = sauce_id[0]
				else:
					sauce_id = self.pool.get('sauce.menu').create(cr, uid, {'name': str(sauce)})
			if row<7:
				carb_type='low carb'
			else:
				carb_type='high carb'

			ingg=xl_sheet3.cell(row, 9).value.split(',')
			ing_idd=[]
			for i in ingg:
				if i:
					ing_id = self.pool.get('ingredient.menu').search(cr, uid, [('ingredient_name', 'like', str(i))]) 
				if ing_id:
					ing_id = ing_id[0]
				else:
					ing_id = self.pool.get('ingredient.menu').create(cr, uid, {'ingredient_name': str(i)})
				ing_idd+=[ing_id]

			vals={
				'name':name,
				'meal_preference':'fish',
				'carb':carbs,
				'sauce_id':sauce_id,
				'protein':protein,
				'fat':fat,
				'ingredients':[(6,0,ing_idd)],
				'carb_type':carb_type,
			}
			self.pool.get('recipies.meal').create(cr,uid,vals,context=context)

		xl_sheet4 = xl_workbook.sheet_by_name(sheet_names[4])#accessing the second sheet of workbook
		for row in range(3,xl_sheet4.nrows):
			name = xl_sheet4.cell(row, 1).value
			if 'Steak' in name:
				meal_preference='beef'
			elif 'Bacon' in name:
				meal_preference='beef'
			else:
				meal_preference='veg'

			carbs=xl_sheet4.cell(row, 4).value
			protein=xl_sheet4.cell(row, 5).value
			fat=xl_sheet4.cell(row, 6).value
			
			if carbs<protein:
				carb_type='low carb'
			else:
				carb_type='high carb'
			ingg=xl_sheet4.cell(row, 8).value.split(',')
			ing_idd=[]
			for i in ingg:
				if i:
					ing_id = self.pool.get('ingredient.menu').search(cr, uid, [('ingredient_name', 'like', str(i))]) 
				if ing_id:
					ing_id = ing_id[0]
				else:
					ing_id = self.pool.get('ingredient.menu').create(cr, uid, {'ingredient_name': str(i)})
				ing_idd+=[ing_id]

			vals={
				'name':name,
				'meal_preference':meal_preference,
				'breakfast':True,
				'carb':carbs,
				'protein':protein,
				'fat':fat,
				'ingredients':[(6,0,ing_idd)],
				'carb_type':carb_type,
			}
			self.pool.get('recipies.meal').create(cr,uid,vals,context=context)

		

		return True



	def import_custom_item_details(self,cr,uid,ids,context=None):
		vals={}
		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		xl_workbook = xlrd.open_workbook(BASE_DIR +'/excel/Athleat Customized Meal System Finalized.xlsx')
		sheet_names = xl_workbook.sheet_names()
		
		xl_sheet0 = xl_workbook.sheet_by_name(sheet_names[0])
		for row in range(12,xl_sheet0.nrows):
			if row<21:
				name= xl_sheet0.cell(row, 0).value

				if 'Chicken' in name:
					meal_preference='chick'
				elif 'Beef' in name or 'Angus' in name or 'Meatballs' in name:
					meal_preference='beef'
				elif 'Salmon' in name or 'Tuna' in name or 'NilePerch' in name:
					meal_preference='fish'
				elif 'Tofu' in name:
					meal_preference='veg'
				else:
					pass

				carbs=xl_sheet0.cell(row, 2).value
				protein=xl_sheet0.cell(row, 3).value
				fat=xl_sheet0.cell(row, 4).value
				price=xl_sheet0.cell(row, 6).value
				
				vals={
					'name':name,
					'quantity':'100',
					'meal_preference':meal_preference,
					'carb':carbs,
					'protein':protein,
					'fat':fat,
					'carb_type':'customize',
					'price':price,
				}
				self.pool.get('recipies.meal').create(cr,uid,vals,context=context)

			elif row>22 and row<34:
				name= xl_sheet0.cell(row, 0).value

				carbs=xl_sheet0.cell(row, 2).value
				protein=xl_sheet0.cell(row, 3).value
				fat=xl_sheet0.cell(row, 4).value
				price=xl_sheet0.cell(row, 6).value
				
				vals={
					'name':name,
					'quantity':'100',
					'meal_preference':'veg',
					'carb':carbs,
					'protein':protein,
					'fat':fat,
					'carb_type':'customize',
					'price':price,
				}
				self.pool.get('recipies.meal').create(cr,uid,vals,context=context)

			elif row>35 and row<44:
				name=xl_sheet0.cell(row, 0).value
				protein=xl_sheet0.cell(row, 3).value
				price=xl_sheet0.cell(row, 6).value

				vals={
					'name':name,
					'protein':protein,
					'price':price,
				}
				self.pool.get('addons.conf').create(cr,uid,vals,context=context)
			else:
				pass

		xl_sheet1 = xl_workbook.sheet_by_name(sheet_names[5])
		for row in range(2,xl_sheet1.nrows):
			if row<6:
				name=xl_sheet1.cell(row, 0).value

				if 'Bacon' in name:
					meal_preference=''
				else:
					meal_preference='veg'

				carbs=xl_sheet1.cell(row, 2).value
				protein=xl_sheet1.cell(row, 3).value
				fat=xl_sheet1.cell(row, 4).value
				price=xl_sheet1.cell(row, 6).value
				
				vals={
					'name':name,
					'meal_preference':meal_preference,
					'carb':carbs,
					'protein':protein,
					'fat':fat,
					'carb_type':'customize',
					'price':price,
				}
				self.pool.get('recipies.meal').create(cr,uid,vals,context=context)

			elif row>8 and row<13:
				name=xl_sheet1.cell(row, 0).value
				if 'Angus' in name or 'Steak' in name:
					meal_preference='beef'
				else:
					meal_preference='chick'

				carbs=xl_sheet1.cell(row, 2).value
				protein=xl_sheet1.cell(row, 3).value
				fat=xl_sheet1.cell(row, 4).value
				price=xl_sheet1.cell(row, 6).value
	
				vals={
					'name':name,
					'meal_preference':meal_preference,
					'carb':carbs,
					'protein':protein,
					'fat':fat,
					'carb_type':'customize',
					'price':price,
				}
				self.pool.get('recipies.meal').create(cr,uid,vals,context=context)
			elif row>14 and row<19:
				name=xl_sheet1.cell(row, 0).value
				if 'Angus' in name:
					meal_preference='beef'
				else:
					meal_preference='chick'

				carbs=xl_sheet1.cell(row, 2).value
				protein=xl_sheet1.cell(row, 3).value
				fat=xl_sheet1.cell(row, 4).value
				price=xl_sheet1.cell(row, 6).value
			
				vals={
					'name':name,
					'meal_preference':meal_preference,
					'carb':carbs,
					'protein':protein,
					'fat':fat,
					'carb_type':'customize',
					'price':price,
				}
				self.pool.get('recipies.meal').create(cr,uid,vals,context=context)

			elif row>21 and row<25:
				name=xl_sheet1.cell(row, 0).value
				carbs=xl_sheet1.cell(row, 2).value
				protein=xl_sheet1.cell(row, 3).value
				fat=xl_sheet1.cell(row, 4).value
				price=xl_sheet1.cell(row, 6).value
				
				vals={
					'name':name,
					'meal_preference':'chick',
					'carb':carbs,
					'protein':protein,
					'fat':fat,
					'carb_type':'customize',
					'price':price,
				}
				self.pool.get('recipies.meal').create(cr,uid,vals,context=context)
			else:
				pass	
		return True

	

Recipies()




class Liked_recipies(osv.osv):

	_name="liked.disliked.meals"	
	_rec_name ="name"

	_columns={

	'name':fields.many2one('recipies.meal','Name'),
	'item':fields.many2one('likes.dislikes','Item'),
	'meal_preference':fields.selection([('chick','Chicken'),
										('beef','Beef'),
										('fish','Fish'),
										('lamb','Lamb'),
										('veg','Veg')],'Meal Category'),
	'req_id':fields.many2one('likes.dislikes.requests','Relation ID'),
	}
Liked_recipies()
