from openerp.osv import osv,fields

class Options(osv.osv):
	
	_name = "meal.options"	

	_rec_name = "date"

	_columns = {

	'date': fields.date('Date'),

	'options':fields.many2many('recipies.meal','rel_date_options','option_id','meal_id','Options/Meals'),
	}
	
Options()
