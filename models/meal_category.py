from openerp.osv import osv,fields

class Meal_Category(osv.osv):
	_name="meal.category"

	_rec_name="meal_category_name"
	
	_columns={
	'meal_category_name':fields.char('Meals Category Name'),
	'meal_type': fields.selection([('chick','Chicken'),
								('beef','Beef'),
								('fish','Fish'),
								('lamb','Lamb'),
								('veg','Veg')],"Meal Preference"),
	}
Meal_Category()
