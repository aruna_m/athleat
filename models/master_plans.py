from openerp.osv import osv,fields
from openerp import api
import datetime
from datetime import datetime, timedelta, date
import time,random
from openerp.exceptions import except_orm, Warning
from openerp.tools.translate import _


class Master_meal_plans(osv.osv):

	_name = "master.plans"

	_rec_name = 'meal_plan_type'

	_columns = {

	'meal_plan_type' : fields.selection([('high carb','High Carb'),
											('low carb','Low Carb'),
											('customize','Customize'),],'Meal Plan Type'),
	'meal_type': fields.selection([('veg','Veg'),
								('nonveg','Non Veg')],"Meal Preference"),
	'month': fields.selection([
						('jan', 'January'),
						('feb', 'February'),
						('mar', 'March'),
						('apr', 'April'),
						('may', 'May'),
						('jun', 'June'),
						('jul', 'July'),
						('aug', 'August'),
						('sep', 'September'),
						('oct', 'October'),
						('nov', 'November'),
						('dec', 'December'),
				], 'Month'),
	'week':fields.selection([(1,'Week-1'),
							(2,'Week-2'),
							(3,'Week-3'),
							(4,'Week-4')],'Week'),
	'week1_meals_id' : fields.one2many('meal.meal','master_week1id','Week-1 Meal Plan'),
	'week2_meals_id' : fields.one2many('meal.meal','master_week2id','Week-2 Meal Plan'),
	'week3_meals_id' : fields.one2many('meal.meal','master_week3id','Week-3 Meal Plan'),
	'week4_meals_id' : fields.one2many('meal.meal','master_week4id','Week-4 Meal Plan'),

	'cust_week1_meals_id':fields.one2many('meal.meal','cust_master_week1id','Week-1 Customized Meal Plan'),
	'cust_week2_meals_id':fields.one2many('meal.meal','cust_master_week2id','Week-2 Customized Meal Plan'),
	'cust_week3_meals_id':fields.one2many('meal.meal','cust_master_week3id','Week-3 Customized Meal Plan'),
	'cust_week4_meals_id':fields.one2many('meal.meal','cust_master_week4id','Week-4 Customized Meal Plan'),
	}

	_sql_constraints = [
		('uniq_master_plan', 'unique (meal_plan_type,meal_type,month)',
		'Master meal plan with given preferences already exist for this month!!'),

		]


	@api.onchange('week1_meals_id','week2_meals_id','week3_meals_id','week4_meals_id')
	def _onchange_amount(self):
		
		if self.week1_meals_id:
			days =[]
			for i in self.week1_meals_id:
				if i.day in days:raise osv.except_osv(_('Error!'),_('Meals for this day already exists!!'))
				else:days +=[ i.day]

		if self.week2_meals_id:
			days =[]
			for i in self.week2_meals_id:
				if i.day in days:raise osv.except_osv(_('Error!'),_('Meals for this day already exists!!'))
				else:days +=[ i.day]
		
		if self.week3_meals_id:
			days =[]
			for i in self.week3_meals_id:
				if i.day in days:raise osv.except_osv(_('Error!'),_('Meals for this day already exists!!'))
				else:days +=[ i.day]
		
		if self.week4_meals_id:
			days =[]
			for i in self.week4_meals_id:
				if i.day in days:raise osv.except_osv(_('Error!'),_('Meals for this day already exists!!'))
				else:days +=[ i.day]
		
		
	def copy(self, cr, uid, id, default=None, context=None):
		if not default : default = {}

		months = ['jan','feb','mar','apr','may','june','jul','aug','sep','oct','nov','dec']
		mth_index = months.index(self.browse(cr,uid,id).month)
		meal_type = self.browse(cr,uid,id).meal_type

		exist_months = []
		for each in self.search(cr,uid,[('meal_type','=',meal_type)]): exist_months += [str(self.browse(cr,uid,each).month)]

		if mth_index <11 : mth_index+=1
		else: mth_index = 0

		if exist_months:
			for each in exist_months:
				if months[mth_index] in exist_months:
					if mth_index <11:mth_index+=1
					else:mth_index = 0
				else: 
					default.update({'month':months[mth_index]})
					return super(Master_meal_plans, self).copy(cr, uid, id, default, context=context)
	

	def write(self,cr,uid,ids,vals,context=None):

		child_details=[]
		master_details=[]
		final={}
		rec= self.browse(cr,uid,ids)
		plan_rec=self.pool.get('meal.plans')
		weekly_rec=self.pool.get('weekly.plans')
		meal_rec=self.pool.get('meal.meal')
		recipies_rec=self.pool.get('recipies.meal')
		meal_type = rec.meal_type
		carb_type = rec.meal_plan_type
		master_month = rec.month
		rex =0
		wk_meals=[]
		meals_id=['week1_meals_id','week2_meals_id','week3_meals_id','week4_meals_id']
		for x in meals_id:
			if x in vals:
				meals = vals[x]
				for i in meals:

					if i[2] != False:
						
						if 'item1' in i[2]:final.update({'index': meals.index(i),'week':(meals_id.index(x))+1 ,'item1': i[2]['item1']})
						if 'item2' in i[2]:final.update({'index': meals.index(i),'week':(meals_id.index(x))+1 ,'item2': i[2]['item2']})
						if 'item3' in i[2]:final.update({'index': meals.index(i),'week':(meals_id.index(x))+1 ,'item3': i[2]['item3']})
						if 'item4' in i[2]:final.update({'index': meals.index(i),'week':(meals_id.index(x))+1 ,'item4': i[2]['item4']})
				 
						master_details += [final]		
	
		
		months=[]
		meal_plans = plan_rec.search(cr,uid,[('customer.meal_type','=',meal_type),('meal_plan_type','=',carb_type),('state','=','active')])
		weekly_plans = weekly_rec.search(cr,uid,[('cust.meal_type','=',meal_type),('meal_plan_type','=',carb_type)])
	
		for each in meal_plans:
			
			start_date =  datetime.strptime(plan_rec.browse(cr,uid,each).current_date, '%Y-%m-%d')
			month = start_date.strftime("%b").lower()

			weeks = plan_rec.browse(cr,uid,each).no_of_weeks

			cust_id =  plan_rec.browse(cr,uid,each).customer.id
			wk_rec = weekly_rec.search(cr,uid,[('cust','=',cust_id)])
			if wk_rec: rex = weekly_rec.browse(cr,uid,wk_rec[0])#respective weekly plan

			if weeks == 4:
				months += [month]
			elif weeks == 8:
				next_mon1 = (start_date+timedelta(weeks=4)).strftime("%b").lower()
				months += [month,next_mon1]
			elif weeks == 12:
				next_mon1 = (start_date+timedelta(weeks=4)).strftime("%b").lower()
				next_mon2 = (start_date+timedelta(weeks=8)).strftime("%b").lower()
				months += [month,next_mon1,next_mon2]
			else:pass

			mnths=list(set(months))
			if master_month in mnths:
				index = mnths.index(master_month)
				if index == 0: meals = [plan_rec.browse(cr,uid,each).week1_meal_plan.ids , plan_rec.browse(cr,uid,each).week2_meal_plan.ids, plan_rec.browse(cr,uid,each).week3_meal_plan.ids, plan_rec.browse(cr,uid,each).week4_meal_plan.ids]
				if index == 1:meals = [plan_rec.browse(cr,uid,each).week5_meal_plan.ids , plan_rec.browse(cr,uid,each).week6_meal_plan.ids, plan_rec.browse(cr,uid,each).week7_meal_plan.ids, plan_rec.browse(cr,uid,each).week8_meal_plan.ids]
				if index == 2:meals = [plan_rec.browse(cr,uid,each).week9_meal_plan.ids , plan_rec.browse(cr,uid,each).week10_meal_plan.ids, plan_rec.browse(cr,uid,each).week11_meal_plan.ids, plan_rec.browse(cr,uid,each).week12_meal_plan.ids]

				if rex:
					if index == 0: wk_meals = [rex.meals_id1.ids,rex.meals_id2.ids,rex.meals_id3.ids,rex.meals_id4.ids]
					if index == 1: wk_meals = [rex.meals_id5.ids,rex.meals_id6.ids,rex.meals_id7.ids,rex.meals_id8.ids]
					if index == 2: wk_meals = [rex.meals_id9.ids,rex.meals_id10.ids,rex.meals_id11.ids,rex.meals_id12.ids]
				
				meal_count = plan_rec.browse(cr,uid,each).meals_per_day
			
				for ml in meals:
					for mls in ml:
						if wk_meals:
							for j in wk_meals:
								for ek in j:
									for master in master_details:
										if master and 'week' in master:

											# meal plan changes
											if master['week'] == (meals.index(ml))+1 and master['index']+1 == meal_rec.browse(cr,uid,mls).day:
												
												if 'item1' in master:
													if master['item1']==meal_rec.browse(cr,uid,mls).item1.id:pass
													else:
														if plan_rec.browse(cr,uid,each).breakfast:  meal_rec.write(cr,uid,mls,{'item1':master['item1']})
														
												if 'item2' in master:
													if master['item2']==meal_rec.browse(cr,uid,mls).item2.id:pass
													else:
														if plan_rec.browse(cr,uid,each).mid:  meal_rec.write(cr,uid,mls,{'item2':master['item2']})

												if 'item3' in master:
													if master['item3']==meal_rec.browse(cr,uid,mls).item3.id:pass
													else:
														if plan_rec.browse(cr,uid,each).evening:  meal_rec.write(cr,uid,mls,{'item3':master['item3']})

												if 'item4' in master:
													if master['item4']==meal_rec.browse(cr,uid,mls).item4.id:pass
													else:
														if plan_rec.browse(cr,uid,each).night:  meal_rec.write(cr,uid,mls,{'item4':master['item4']})

											# scheduled plan changes 
											if master['week'] == (wk_meals.index(j))+1 and master['index']+1 == meal_rec.browse(cr,uid,ek).day:
												
												if 'item1' in master:
													if master['item1']==meal_rec.browse(cr,uid,ek).item1.id:pass
													else:
														if plan_rec.browse(cr,uid,each).breakfast: meal_rec.write(cr,uid,ek,{'item1':master['item1']})

												if 'item2' in master:
													if master['item2']==meal_rec.browse(cr,uid,ek).item2.id:pass
													else:
														if plan_rec.browse(cr,uid,each).mid:  meal_rec.write(cr,uid,ek,{'item2':master['item2']})
													
												if 'item3' in master:
													if master['item3']==meal_rec.browse(cr,uid,ek).item3.id:pass
													else:
														if plan_rec.browse(cr,uid,each).evening: meal_rec.write(cr,uid,ek,{'item3':master['item3']})

												if 'item4' in master:
													if master['item4']==meal_rec.browse(cr,uid,ek).item4.id:pass
													else:
														if plan_rec.browse(cr,uid,each).night:  meal_rec.write(cr,uid,ek,{'item4':master['item4']})
				
		return super(Master_meal_plans,self).write(cr,uid,ids,vals,context=context)
	
Master_meal_plans()
