from openerp.osv import osv,fields

class Addons(osv.osv):
	
	_name = "addons.conf"	

	_rec_name = "name"

	_columns = {
	'name':fields.char('Name'),
	'meals_id':fields.many2one('meal.meal','Relation to Meals'),
	'hist_meals_id':fields.many2one('history.meal','Relation to History Meals'),
	'master_addons_id':fields.many2one('master.plans','Relation to Master Plans'),
	'price':fields.float('Price'),
	'description': fields.text('Description'),
	}
	
Addons()
