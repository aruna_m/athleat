from openerp.osv import osv,fields
import random
from datetime import datetime, timedelta, date
import time
from itertools import repeat
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp import api, SUPERUSER_ID
# import HTML
from openerp.tools.translate import _

class weekly_plans(osv.osv):

	_name = "weekly.plans"

	_rec_name = 'cust'

	_columns = {
		# 'cust':fields.many2one('res.partner','Customer', domain="[('customer','=',True)]"),
		# 'meals_id1':fields.one2many('meal.meal','weekly_id1','Weekly Meals'),
		# 'meals_id2':fields.one2many('meal.meal','weekly_id2','Weekly Meals'),
		# 'meals_id3':fields.one2many('meal.meal','weekly_id3','Weekly Meals'),
		# 'meals_id4':fields.one2many('meal.meal','weekly_id4','Weekly Meals'),
		# 'meals_id5':fields.one2many('meal.meal','weekly_id5','Weekly Meals'),
		# 'meals_id6':fields.one2many('meal.meal','weekly_id6','Weekly Meals'),
		# 'meals_id7':fields.one2many('meal.meal','weekly_id7','Weekly Meals'),
		# 'meals_id8':fields.one2many('meal.meal','weekly_id8','Weekly Meals'),
		# 'meals_id9':fields.one2many('meal.meal','weekly_id9','Weekly Meals'),
		# 'meals_id10':fields.one2many('meal.meal','weekly_id10','Weekly Meals'),
		# 'meals_id11':fields.one2many('meal.meal','weekly_id11','Weekly Meals'),
		# 'meals_id12':fields.one2many('meal.meal','weekly_id12','Weekly Meals'),

		# 'week_meals_id':fields.one2many('meal.meal','weekid','Meals'),

		# 'start_date': fields.date('Start date'),
		# 'stop_date': fields.date('Stop date'),

		# 'state': fields.selection([('draft','Draft'),
		# 						('confirm','Confirm'),
		# 						('cancelled','Cancelled'),],'Status',),
		# 'no_of_weeks': fields.selection([(1,'1'),
		# 									(2,'2'),
		# 									(4,'4'),
		# 									(8,'8'),
		# 									(12,'12')],'How many Weeks ?'),
		# 'days_per_week': fields.selection([(1,'1'),
		# 									(2,'2'),
		# 									(3,'3'),
		# 									(4,'4'),
		# 									(5,'5')],'How many days per week ?'),
		# 'meals_per_day':fields.selection([(1,'1'),
		# 								(2,'2'),
		# 								(3,'3'),
		# 								(4,'4')],'No of meals per day'),
		# 'current_date': fields.date('Starting when'),
		# 'expiry_date': fields.date('Expiring On'),
		# 'meal_plan_type':fields.selection([('high carb','High Carb'),
		# 									('low carb','Low Carb'),
		# 									('customize','Customize'),],'Meal Plan Type'),
		# 'delivery_timings':fields.selection([('11am-1.30pm','11am-1.30pm'),
		# 									('4pm-6pm','4pm-6pm'),
		# 									('7:45pm-9:45pm','7:45pm-9:45pm')],'Delivery Timings'),
		'meal_plan_id' :fields.many2one('meal.plans','Meal Plan'),
		'cust':fields.related('meal_plan_id','customer',type="many2one", string="Customer", store=True ,relation='res.partner', domain="[('customer','=',True)]"),
		'meals_id1':fields.one2many('meal.meal','weekly_id1','Weekly Meals'),
		'meals_id2':fields.one2many('meal.meal','weekly_id2','Weekly Meals'),
		'meals_id3':fields.one2many('meal.meal','weekly_id3','Weekly Meals'),
		'meals_id4':fields.one2many('meal.meal','weekly_id4','Weekly Meals'),
		'meals_id5':fields.one2many('meal.meal','weekly_id5','Weekly Meals'),
		'meals_id6':fields.one2many('meal.meal','weekly_id6','Weekly Meals'),
		'meals_id7':fields.one2many('meal.meal','weekly_id7','Weekly Meals'),
		'meals_id8':fields.one2many('meal.meal','weekly_id8','Weekly Meals'),
		'meals_id9':fields.one2many('meal.meal','weekly_id9','Weekly Meals'),
		'meals_id10':fields.one2many('meal.meal','weekly_id10','Weekly Meals'),
		'meals_id11':fields.one2many('meal.meal','weekly_id11','Weekly Meals'),
		'meals_id12':fields.one2many('meal.meal','weekly_id12','Weekly Meals'),

		'current_date':fields.related('meal_plan_id','current_date',type="date", string="Starting when", store=True ),
		'expiry_date':fields.related('meal_plan_id','expiry_date',type="date", string="Expiring On", store=True ),
		'no_of_weeks':fields.related('meal_plan_id','no_of_weeks',type="integer", string="How many Weeks ?", store=True ),
		'days_per_week':fields.related('meal_plan_id','days_per_week',type="integer", string="How many days per week ?", store=True ),
		'meals_per_day':fields.related('meal_plan_id','meals_per_day',type="integer", string="No of meals per day", store=True ),
		'meal_plan_type':fields.related('meal_plan_id','meal_plan_type',type="char", string="Meal Plan Type", store=True ),
		'delivery_timings':fields.related('meal_plan_id','delivery_timings',type="char", string="Delivery Timings", store=True ),

		'state': fields.selection([('draft','Draft'),
								('confirm','Confirm'),
								('cancelled','Cancelled'),],'Status',),
	
	}

	_defaults = {

		'state': 'draft',
	}	

	#buttons functionality while click buttom
	def confirm_button(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'state': 'confirm'})

	def cancelled_button(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'state': 'cancelled'})

	def unlink(self, cr, uid, ids, context=None):

		for obj in self.browse(cr, uid,ids, context):
			if obj.state == 'confirm':

				raise Warning(_('Warning!!'), _('You cannot delete a scheduled plan which is already in Confirmed state!!'))

		return super(weekly_plans, self).unlink(cr, uid, ids, context)

	
	def meals_selection_monthly(self,cr,uid,context=None):
		meal_plan_obj = self.pool.get('meal.plans')
		meal_meal_obj = self.pool.get('meal.meal')
		meal_plan_item_obj = self.pool.get('meal.plan.items')

		active_meal_plans = meal_plan_obj.search(cr,uid,[('state','not in',['cancelled','draft'])])
		for i in active_meal_plans:
			meals_id, items = [], []
			pause, resume,paused_days = False, False, []
			list_cust = {'week1':[],'week2':[],'week3':[],'week4':[],'week5':[],'week6':[],'week7':[],'week8':[],'week9':[],'week10':[],'week11':[],'week12':[],}

			rec = meal_plan_obj.browse(cr,uid,i)
			cust = rec.customer.id
			carb_type = rec.meal_plan_type
			weeks = rec.no_of_weeks
			spl_req = rec.spl_req
			addons = [(6,0,rec.addons.ids)]
			likes2 = [(6,0,rec.customer.liked_meals2_ids.ids)]
			dislikes2 = [(6,0,rec.customer.disliked_meals2_ids.ids)]
			
			if rec.current_date:
				date = datetime.strptime(rec.current_date, '%Y-%m-%d')
				prime_day = date.weekday()

			if rec.pause_date : pause = datetime.strptime(rec.pause_date, '%Y-%m-%d')
			if rec.resume_date : resume = datetime.strptime(rec.resume_date, '%Y-%m-%d')
			if rec.expiry_date : expiry = datetime.strptime(rec.expiry_date, '%Y-%m-%d')
		
			if resume and pause:
				flag2 = 1
			# 	for n in range(int ((resume - pause).days)): 
			# 		paused_days += [pause+ timedelta(n)]

			if carb_type == 'customize': 
				flag=1
				day1,day2,day3,day4,day5 =[],[],[],[],[]
				m11, m12, m13, m14=False, False, False, False
				m21, m22, m23, m24=False, False, False, False
				m31, m32, m33, m34=False, False, False, False
				m41, m42, m43, m44=False, False, False, False
				m51, m52, m53, m54=False, False, False, False

				for test in rec.meal_plan_customized.ids:
					day = meal_meal_obj.browse(cr,uid,test).day
					meal_no = meal_meal_obj.browse(cr,uid,test).meal_no
					if day==1 :
						if meal_no ==1:m11=test
						if meal_no ==2:m12=test
						if meal_no ==3:m13=test
						if meal_no ==4:m14=test
						day1=[m11,m12,m13,m14]
					if day==2 : 
						if meal_no ==1:m21=test
						if meal_no ==2:m22=test
						if meal_no ==3:m23=test
						if meal_no ==4:m24=test
						day2=[m21,m22,m23,m24]
					if day==3 : 
						if meal_no ==1:m31=test
						if meal_no ==2:m32=test
						if meal_no ==3:m33=test
						if meal_no ==4:m34=test
						day3=[m31,m32,m33,m34]
					if day==4 : 
						if meal_no ==1:m41=test
						if meal_no ==2:m42=test
						if meal_no ==3:m43=test
						if meal_no ==4:m44=test
						day4=[m41,m42,m43,m44]
					if day==5 : 
						if meal_no ==1:m51=test
						if meal_no ==2:m52=test
						if meal_no ==3:m53=test
						if meal_no ==4:m54=test
						day5=[m51,m52,m53,m54]

				meal_rec = [[day1],[day2],[day3],[day4],[day5]]* weeks
				loop=xrange(rec.no_of_weeks)
			elif carb_type in ['high carb','low carb']:
				flag=0
				meal_rec = [rec.week1_meal_plan.ids, rec.week2_meal_plan.ids, rec.week3_meal_plan.ids, rec.week4_meal_plan.ids, rec.week5_meal_plan.ids, rec.week6_meal_plan.ids, rec.week7_meal_plan.ids,rec.week8_meal_plan.ids, rec.week9_meal_plan.ids, rec.week10_meal_plan.ids, rec.week11_meal_plan.ids, rec.week12_meal_plan.ids]
				loop=xrange(rec.no_of_weeks/4)
			else:pass

			no, wkno, wk = 1, 1, 1
			intermediate={}
			replacable = {'week-1':[],'week-2':[],'week-3':[],'week-4':[],'week-5':[],'week-6':[],'week-7':[],'week-8':[],'week-9':[],'week-10':[],'week-11':[],'week-12':[]}
			
			try:	
				for meal in meal_rec:
					total_items_names = ''
					total_items = []

					if meal :
						for i in meal:
							# print "dateeeeeeeeeeeeeeeeeee",date
							if (pause and not resume and date == pause) or (date > expiry): break
							elif pause and resume and date == pause: date = resume
							else:pass

							if carb_type in ['high carb','low carb']:
								item1 = meal_meal_obj.browse(cr,uid,i).item1.id
								item2 = meal_meal_obj.browse(cr,uid,i).item2.id
								item3 = meal_meal_obj.browse(cr,uid,i).item3.id
								item4 = meal_meal_obj.browse(cr,uid,i).item4.id
								intermediate = {'spl_req':spl_req,'liked_meals2_ids':likes2,'disliked_meals2_ids':dislikes2,'addons':addons, 'cust':cust, 'carb_type':carb_type, 'item1':item1,'item2':item2,'item3':item3,'item4':item4,'date':date, 'status':'scheduled', 'week':'week-'+str(no),'day':meal.index(i)+1}

							if carb_type == 'customize': 
								B, M, E, N = rec.breakfast, rec.mid, rec.evening, rec.night
								intermediate={'spl_req':spl_req,'liked_meals2_ids':likes2,'disliked_meals2_ids':dislikes2,'addons':addons, 'cust':cust, 'carb_type':carb_type, 'date':date, 'status':'scheduled', 'week':'week-'+str(wk),'day':wkno,'meal11':False,'meal12':False,'meal13':False,'sauce1':False,'meal21':False,'meal22':False,'meal23':False,'sauce2':False,'meal31':False,'meal32':False,'meal33':False,'sauce3':False,'meal41':False,'meal42':False,'meal43':False,'sauce4':False}
								x=0
								for ml in i:
									x+=1
									mells = meal_meal_obj.browse(cr,uid,ml).plan_meal_items.ids
									for mm in mells:
										rcc=meal_plan_item_obj.browse(cr,uid,mm)
										if rcc.item_id : intermediate.update({'meal'+str(i.index(ml)+1)+str(mells.index(mm)+1):str(rcc.item_id.name)+'('+str(rcc.grams)+'Gms)'+'('+str(rcc.protein)+'P-'+str(rcc.carb)+'C-'+str(rcc.fat)+'F)'})	
									if meal_meal_obj.browse(cr,uid,ml).extra_sauce : intermediate.update({'sauce'+str(i.index(ml)+1):str(meal_meal_obj.browse(cr,uid,ml).extra_sauce.name)})
							meals_id = meals_id + [(0,0,intermediate )]
							
							replacable[intermediate['week']] += [intermediate]
					
							wkno+=1
							if wkno>5:
								wkno=1
								wk+=1

							# if pause and resume and flag2 == 1:
							# 	flag2 = 0
							# 	if date == pause:date = resume
							# elif pause and not resume and date == pause: 
							# 	break
							# else:
							if date.weekday() == 2:  date = date + timedelta(days=3)
							elif date.weekday() == 3: date = date + timedelta(days=2)
							elif date.weekday() == 4:  date = date + timedelta(days=1)
							else:  date = date + timedelta(days=1)
						
							items=[]
					
					else:
						for ir in xrange(rec.days_per_week):

							if (pause and not resume and date == pause) or (date > expiry): break
							elif pause and resume and date == pause: date = resume
							else:pass

							meals_id = meals_id + [(0,0, {'spl_req':spl_req,'liked_meals2_ids':likes2,'disliked_meals2_ids':dislikes2,'addons':addons,'cust':cust,'item1':False,'item2':False,'item3':False,'item4':False, 'date':date, 'status':'scheduled', 'week':'week-'+str(no),})]
							
							# if pause and resume and flag2 == 1:
							# 	flag2 = 0
							# 	date = resume
							# elif pause and not resume and date == pause: 
							# 	break
							# else:
							if date.weekday() == 2: date = date + timedelta(days=3)
							elif date.weekday() == 3: date = date + timedelta(days=2)
							elif date.weekday() == 4: date = date + timedelta(days=1)
							else: date = date + timedelta(days=1)

					no+=1
				


				for each in meals_id:
					if each[2]['week'] == 'week-1': list_cust['week1'] += [each]
					elif each[2]['week'] == 'week-2': list_cust['week2'] += [each]
					elif each[2]['week'] == 'week-3': list_cust['week3'] += [each]
					elif each[2]['week'] == 'week-4': list_cust['week4'] += [each]
					elif each[2]['week'] == 'week-5': list_cust['week5'] += [each]
					elif each[2]['week'] == 'week-6': list_cust['week6'] += [each]
					elif each[2]['week'] == 'week-7': list_cust['week7'] += [each]
					elif each[2]['week'] == 'week-8': list_cust['week8'] += [each]
					elif each[2]['week'] == 'week-9': list_cust['week9'] += [each]
					elif each[2]['week'] == 'week-10': list_cust['week10'] += [each]
					elif each[2]['week'] == 'week-11': list_cust['week11'] += [each]
					elif each[2]['week'] == 'week-12': list_cust['week12'] += [each]
					else: pass
				

				# for li in xrange(0, len(meals_id), rec.days_per_week):

				# 	list_cust =  list_cust+[meals_id[li:li + rec.days_per_week]]
				
				exists = self.search(cr,uid,[('cust', '=', cust)],context=context)
				if exists:
					vals={}
					# vals={'meals_id1':[],'meals_id2':[],'meals_id3':[],'meals_id4':[],'meals_id5':[],'meals_id6':[],'meals_id7':[],'meals_id8':[],'meals_id9':[],'meals_id10':[],'meals_id11':[],'meals_id12':[]}
					# self.write(cr, uid, exists,{'meals_per_day':rec.meals_per_day, 'meal_plan_type':carb_type, 'current_date':rec.current_date, 'expiry_date':rec.expiry_date, 'no_of_weeks':rec.no_of_weeks, 'days_per_week':rec.days_per_week}, context=context)

					req_id = False
					test = []

					existing = self.browse(cr,uid,exists[0])

					x,y = [existing.meals_id1.ids,existing.meals_id2.ids,existing.meals_id3.ids,existing.meals_id4.ids,existing.meals_id5.ids,existing.meals_id6.ids,existing.meals_id7.ids,existing.meals_id8.ids,existing.meals_id9.ids,existing.meals_id10.ids,existing.meals_id11.ids,existing.meals_id12.ids],['week-1','week-2','week-3','week-4','week-5','week-6','week-7','week-8','week-9','week-10','week-11','week-12']

					for each in x:
						test += each
					req_id = test[-1]
				
					if req_id:
					# if req_id and rec.expiry_date != self.pool.get('meal.meal').browse(cr,uid,req_id).date:
						x,y = [existing.meals_id1.ids,existing.meals_id2.ids,existing.meals_id3.ids,existing.meals_id4.ids,existing.meals_id5.ids,existing.meals_id6.ids,existing.meals_id7.ids,existing.meals_id8.ids,existing.meals_id9.ids,existing.meals_id10.ids,existing.meals_id11.ids,existing.meals_id12.ids],['week-1','week-2','week-3','week-4','week-5','week-6','week-7','week-8','week-9','week-10','week-11','week-12']
						for i, j in zip(x,y):
							records = []

							if len(i) < len(replacable[j]): 
								for valz in i:
									for each in replacable[j][0:len(i)]:
										if replacable[j].index(each) == i.index(valz):
											records += [(1,valz,each)]
								for rest in replacable[j][len(i):]:
									records += [(0,0,rest)]
							elif len(i) > len(replacable[j]):
								for valz in i[0:len(replacable[j])]:
									for each in replacable[j]:
										if replacable[j].index(each) == i.index(valz):
											records += [(1,valz,each)]
								self.pool.get('meal.meal').browse(cr,uid,i[len(replacable[j]):]).unlink()
							else: 
								for valz in i:
									for each in replacable[j]:
										if replacable[j].index(each) == i.index(valz):
											records += [(1,valz,each)]

							if j == 'week-1':vals.update({'meals_id1':records})
							elif j == 'week-2':vals.update({'meals_id2':records})
							elif j == 'week-3':vals.update({'meals_id3':records})
							elif j == 'week-4':vals.update({'meals_id4':records})
							elif j == 'week-5':vals.update({'meals_id5':records})
							elif j == 'week-6':vals.update({'meals_id6':records})
							elif j == 'week-7':vals.update({'meals_id7':records})
							elif j == 'week-8':vals.update({'meals_id8':records})
							elif j == 'week-9':vals.update({'meals_id9':records})
							elif j == 'week-10':vals.update({'meals_id10':records})
							elif j == 'week-11':vals.update({'meals_id11':records})
							elif j == 'week-12':vals.update({'meals_id12':records})
							else:pass

					self.write(cr, uid, existing.id,{'meal_plan_id':rec.id,
							'meals_id1':vals['meals_id1'],'meals_id2':vals['meals_id2'],
							'meals_id3':vals['meals_id3'],'meals_id4':vals['meals_id4'],
							'meals_id5':vals['meals_id5'],'meals_id6':vals['meals_id6'],
							'meals_id7':vals['meals_id7'],'meals_id8':vals['meals_id8'],
							'meals_id9':vals['meals_id9'],'meals_id10':vals['meals_id10'],
							'meals_id11':vals['meals_id11'],'meals_id12':vals['meals_id12']}, context=context)

				else:
					
					if weeks == 12: self.create(cr, uid, {'meal_plan_id':rec.id,'meals_id1':list_cust['week1'],'meals_id2':list_cust['week2'],'meals_id3':list_cust['week3'],'meals_id4':list_cust['week4'],'meals_id5':list_cust['week5'],'meals_id6':list_cust['week6'],'meals_id7':list_cust['week7'],'meals_id8':list_cust['week8'],'meals_id9':list_cust['week9'],'meals_id10':list_cust['week10'],'meals_id11':list_cust['week11'],'meals_id12':list_cust['week12']}, context=context)
					elif weeks == 4: self.create(cr, uid, {'meal_plan_id':rec.id,'meals_id1':list_cust['week1'],'meals_id2':list_cust['week2'],'meals_id3':list_cust['week3'],'meals_id4':list_cust['week4'],}, context=context)
					elif weeks == 1: self.create(cr, uid, {'meal_plan_id':rec.id,'meals_id1':list_cust['week1']}, context=context)
					elif weeks == 2: self.create(cr, uid, {'meal_plan_id':rec.id,'meals_id1':list_cust['week1'],'meals_id2':list_cust['week2']}, context=context)
					elif weeks == 8: self.create(cr, uid, {'meal_plan_id':rec.id,'meals_id1':list_cust['week1'],'meals_id2':list_cust['week2'],'meals_id3':list_cust['week3'],'meals_id4':list_cust['week4'],'meals_id5':list_cust['week5'],'meals_id6':list_cust['week6'],'meals_id7':list_cust['week7'],'meals_id8':list_cust['week8'],}, context=context)
					else:pass
				
				
			except Exception as e:
				print e
			
		
weekly_plans()


