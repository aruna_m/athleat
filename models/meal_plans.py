from openerp.osv import osv,fields
from openerp import api
import datetime
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import dateutil.relativedelta as REL
from openerp.tools.translate import _
import time,random
import collections
from dateutil.rrule import *
from openerp.exceptions import except_orm, Warning, RedirectWarning
import math
import urllib
import urllib2

def date_by_adding_business_days(from_date, add_days):
	business_days_to_add = add_days
	current_date = from_date
	while business_days_to_add > 0:
		current_date += timedelta(days=1)
		weekday = current_date.weekday()
		if from_date.weekday() == 6:
			if weekday == 3 or weekday == 4: # sunday = 6
				continue
		if from_date.weekday() == 5:
			if weekday == 3 or weekday == 4: # sunday = 6
				continue
		business_days_to_add -= 1
	return current_date

# def get_next_weekday(startdate, weekday):
# 	"""
# 	@startdate: given date, in format '2013-05-25'
# 	"""
# 	d = datetime.strptime(startdate, '%Y-%m-%d')
# 	if weekday == 'Saturday':
# 		rd = relativedelta(days=1, weekday=REL.SA)
# 	else:
# 		rd = relativedelta(days=1, weekday=REL.SU)

# 	next_saturday = d + rd
# 	return next_saturday

# def date_by_adding_business_days(from_date, add_days):
# 	business_days_to_add = add_days
# 	current_date = from_date
# 	while business_days_to_add > 0:
# 		current_date += timedelta(days=1)
# 		weekday = current_date.weekday()
# 		if from_date.weekday() == 6:
# 			if weekday == 5 or weekday == 4: # sunday = 6
# 				continue
		
# 		business_days_to_add -= 1
# 	return current_date

def get_next_weekday(startdate, weekday):
	"""
	@startdate: given date, in format '2013-05-25'
	"""
	d = datetime.strptime(startdate, '%Y-%m-%d')
	if weekday == 'Saturday':
		rd = relativedelta(days=1, weekday=REL.SA)
	
	next_saturday = d + rd
	return next_saturday

class Meal_plans(osv.osv):

	_name="meal.plans"

	_rec_name = 'customer'

	_order = "id desc"
	_inherit = ['ir.needaction_mixin']

	def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
		res = {}
		for meal in self.browse(cr, uid, ids, context=context):
			val=quantity=0.0
			
			res[meal.id] = {
					'total_amount': 0.0,
					'total_weight': 0.0,
				}
			for line in meal.week1_meal_plan+meal.week2_meal_plan+meal.week3_meal_plan+meal.week4_meal_plan:
				val+=line.price
				quantity+=line.quantity
	
			res[meal.id]['amount_total'] = val
			res[meal.id]['total_weight'] = quantity

		return res

	
	def _get_plan_days_left(self, cr, uid, ids, field_name, arg, context=None):
		res = {}
		for meal in self.browse(cr, uid, ids, context=context):
			days_left = 0
			counter = 0
			paused_days = 0
			paused_days_counter = 0
			res[meal.id] = {
					'plan_days_left': 0,
					'paused_days': 0,
					# 'expiry_date': False,
				}
			if meal.pause_date and meal.resume_date:
				pause_date = datetime.strptime(meal.pause_date, '%Y-%m-%d').date()
				resume_date = datetime.strptime(meal.resume_date, '%Y-%m-%d').date()
				paused_days = (resume_date - pause_date).days

				for i in range(paused_days+1):
					each = pause_date + timedelta(days=i)
					if each.weekday() not in [4,5]:
						paused_days_counter += 1
			if meal.current_date and meal.expiry_date:
				current_date = datetime.strptime(meal.current_date, '%Y-%m-%d').date()
			# 	if not meal.pause_date and not meal.resume_date:
			# 		res[meal.id]['expiry_date'] = date_by_adding_business_days(current_date, ((meal.no_of_weeks*meal.days_per_week)-1))
			# 	else:
			# 		prev_expiry_date = date_by_adding_business_days(current_date, ((meal.no_of_weeks*meal.days_per_week)-1))
			# 		res[meal.id]['expiry_date'] = date_by_adding_business_days(prev_expiry_date, (paused_days_counter))
			
			# if res[meal.id]['expiry_date']:
				expiry_date = datetime.strptime(meal.expiry_date, '%Y-%m-%d').date()
				if current_date < datetime.now().date():
					days_left = (expiry_date - datetime.now().date()).days
					for i in range(days_left + 1):
						if meal.resume_date:
							resume_date = datetime.strptime(meal.resume_date, '%Y-%m-%d').date()
							if current_date <= resume_date <= expiry_date:
								each = resume_date + timedelta(days=i)
						else:
							each = datetime.now().date() + timedelta(days=i)
						if each.weekday() not in [4,5]:
							counter += 1
				else:
					# days_left = (expiry_date - current_date).days
					# for i in range(days_left + 1):
						# each = current_date + timedelta(days=i)
						# if each.weekday() not in [4,5]:
					# counter += 1
					counter = meal.no_of_weeks*meal.days_per_week
			res[meal.id]['plan_days_left'] = counter
			res[meal.id]['paused_days'] = paused_days_counter
		return res
	

	_columns={
		'name':fields.char('Sequence ID'),
		'customer':fields.many2one('res.partner','Customer'),
		# 'customer':fields.many2one('res.partner','Customer', domain="[('customer','=',True)]"),
		'state': fields.selection([('draft','Draft'),
								('active','Active'),
								('pending-pause','Pending-Pause'),
								('paused','Paused'),
								('pending-active','Pending-Active'),
								('pending-cancel','Pending-Cancel'),
								('pending-renewal','Pending-Renewal'),
								('renewal-request-sent','Renewal Request Sent'),
								('expired','Expired'),
								('cancelled','Cancelled'),],'Status',),
		'no_of_weeks': fields.selection([(1,'1'),
											(2,'2'),
											(4,'4'),
											(8,'8'),
											(12,'12')],'How many Weeks ?'),
		'days_per_week': fields.selection([(1,'1'),
											(2,'2'),
											(3,'3'),
											(4,'4'),
											(5,'5')],'How many days per week ?'),
		'meals_per_day':fields.selection([(1,'1'),
										(2,'2'),
										(3,'3'),
										(4,'4')],'No of meals per day'),
		'plan_price': fields.float('Plan Price'),
		'addons':fields.many2many('addons.conf',id1='meal_id',id2='addon_id',string='Addons'),
		'substitutes':fields.many2many('recipies.meal',id1='plan_id',id2='item_id',string='Substitute Meals'),
		'current_date': fields.date('Starting when'),
		'expiry_date': fields.date('Expiring Date'),
		# 'expiry_date': fields.function(_get_plan_days_left, type='date', string='Expiry Date', help="Expiry Date", multi='sums', store=False),
		'history_meal':fields.one2many('history.meal','rels_id','History Meal',states={'draft': [('readonly', False)]},readonly=True),
	
		'week1_meal_plan':fields.one2many('meal.meal','week1_rel_id','Week1 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week2_meal_plan':fields.one2many('meal.meal','week2_rel_id','Week2 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week3_meal_plan':fields.one2many('meal.meal','week3_rel_id','Week3 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week4_meal_plan':fields.one2many('meal.meal','week4_rel_id','Week4 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week5_meal_plan':fields.one2many('meal.meal','week5_rel_id','Week5 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week6_meal_plan':fields.one2many('meal.meal','week6_rel_id','Week6 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week7_meal_plan':fields.one2many('meal.meal','week7_rel_id','Week7 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week8_meal_plan':fields.one2many('meal.meal','week8_rel_id','Week8 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week9_meal_plan':fields.one2many('meal.meal','week9_rel_id','Week9 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week10_meal_plan':fields.one2many('meal.meal','week10_rel_id','Week10 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week11_meal_plan':fields.one2many('meal.meal','week11_rel_id','Week11 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		'week12_meal_plan':fields.one2many('meal.meal','week12_rel_id','Week12 Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		
		'meal_plan_customized':fields.one2many('meal.meal','plan_rel_id','Meal Includes',states={'draft': [('readonly', False)]},readonly=True),
		
		'meal_plan_type':fields.selection([('high carb','High Carb'),
											('low carb','Low Carb'),
											('customize','Customize'),],'Meal Plan Type'),
		"amount_total":fields.function(_amount_all, type='float', string='Total Price', multi='sums', help="The total Price amount.", store=True),
		"total_weight":fields.function(_amount_all, type='float', string='Total Weight', multi='sums', help="The total Weight of all items.", store=True),
		"plan_days_left":fields.function(_get_plan_days_left, type='integer', string='Days Left in Plan', multi='sums', help="The total days left.", store=False),
		"paused_days":fields.function(_get_plan_days_left, type='integer', string='Paused Days', multi='sums', help="The total days paused."),
		'day':fields.char('Day'),
		'days_left':fields.integer('Days Left'),
		'breakfast':fields.boolean('BREAKFAST'),
		'mid':fields.boolean('MID'),
		'evening':fields.boolean('EVENING'),
		'night':fields.boolean('NIGHT'),
		'spl_req':fields.text('Special Request'),
		'counter':fields.integer('Counter'),
		'delivery_timings':fields.selection([('11am-1.30pm','11am-1.30pm'),
											('4pm-6pm','4pm-6pm'),
											('7:45pm-9:45pm','7:45pm-9:45pm')],'Delivery Timings'),
		'paid': fields.boolean('Paid??'),
		'customer_payments':fields.one2many('customer.payments','plan_id','Customer Payments'),
		'pause_date': fields.date('Pause Date'),
		'resume_date': fields.date('Resume Date'),
		'active': fields.boolean('Active'),
	
		}
	_defaults = {

		'days_per_week':5,
		'meals_per_day':4,
		'state': 'draft',
		'active': True
	}	

	def _needaction_count(self, cr, uid, domain=None, context=None):
		domain = self._needaction_domain_get(self,cr,uid)
		return len(self.search(cr,uid,domain))

	def _needaction_domain_get(self,cr,uid,context=None):
		return [('state', 'in', ['draft'])]


	@api.onchange('customer')
	def _onchange_customer(self):
		try:
			if self.customer.carb_type:
				self.meal_plan_type = self.customer.carb_type
			if self.customer.breakfast:
				self.breakfast = self.customer.breakfast
		except Exception as e:
			pass


	@api.onchange('meals_per_day','no_of_weeks', 'meal_plan_type')
	def _onchange_customer(self):
		if self.meal_plan_type != 'customize':
			if self.meals_per_day == 2:
				if self.no_of_weeks == 2:
					self.plan_price = 995.5
				elif self.no_of_weeks == 4:
					self.plan_price = 1810
				elif self.no_of_weeks == 8:
					self.plan_price = 3493.3
				elif self.no_of_weeks == 12:
					self.plan_price = 5104.2
			
			elif self.meals_per_day == 3:
				if self.no_of_weeks == 2:
					self.plan_price = 1397
				elif self.no_of_weeks == 4:
					self.plan_price = 2540
				elif self.no_of_weeks == 8:
					self.plan_price = 4902.2
				elif self.no_of_weeks == 12:
					self.plan_price = 7162.8
			else:
				if self.no_of_weeks == 2:
					self.plan_price = 1683
				elif self.no_of_weeks == 4:
					self.plan_price = 3060
				elif self.no_of_weeks == 8:
					self.plan_price = 5905.8
				elif self.no_of_weeks == 12:
					self.plan_price = 8629.2
		

	# @api.onchange('breakfast','mid','evening','night','counter')
	# def _onchange_boolean_meals(self):
		
	# 	if self.breakfast : self.counter += 1
	# 	if self.mid : self.counter += 1
	# 	if self.evening : self.counter += 1
	# 	if self.night : self.counter += 1
			
	# 	if self.counter > self.meals_per_day:raise Warning(_('Warning!!'), _('Current Selection does not match with the No. of meals'))
	# 	elif self.counter < self.meals_per_day:raise Warning(_('Warning!!'), _('Current Selection does not match with the No. of meals'))
	# 	else:pass
	
	def write(self, cr, uid, ids, vals, context=None):
		rec = self.browse(cr,uid,ids)
		count=0
		meals=0
		if 'breakfast' in vals:count+=int(vals['breakfast'])
		else: count += int(rec.breakfast)
		if 'mid' in vals:count+=int(vals['mid'])
		else: count += int(rec.mid)
		if 'evening' in vals:count+=int(vals['evening'])
		else: count += int(rec.evening)
		if 'night' in vals:count+=int(vals['night'])
		else: count += int(rec.night)

		if 'meals_per_day' in vals:meals+=vals['meals_per_day']
		else:meals+=rec.meals_per_day

		resume_date = vals.get('resume_date', None)
		pause_date = vals.get('pause_date', None)
		plan_price = vals.get('plan_price', None)
		if resume_date and rec.expiry_date and pause_date:
			resume_date = datetime.strptime(resume_date, '%Y-%m-%d') 
			pause = datetime.strptime(pause_date, '%Y-%m-%d')
			expiry_date = datetime.strptime(rec.expiry_date, '%Y-%m-%d')
			delta = resume_date - pause
			# vals.update({'expiry_date':date_by_adding_business_days(expiry_date, delta.days)})

		if resume_date and rec.pause_date and rec.expiry_date and not pause_date:
			resume_date = datetime.strptime(resume_date, '%Y-%m-%d') 
			pause = datetime.strptime(rec.pause_date, '%Y-%m-%d')
			expiry_date = datetime.strptime(rec.expiry_date, '%Y-%m-%d')
			delta = resume_date - pause
			# vals.update({'expiry_date':date_by_adding_business_days(expiry_date, delta.days)})

		if not plan_price:
			if count != meals:raise Warning(_('Warning!!'), _('Current Selection does not match with the No. of meals'))

		# if 'current_date' in vals:
		# 	if vals['current_date'] and rec.no_of_weeks and rec.days_per_week: 
		# 		vals.update({'expiry_date':date_by_adding_business_days(datetime.strptime(vals['current_date'], '%Y-%m-%d'), ((rec.no_of_weeks*rec.days_per_week)-1))})
		# 	if vals['current_date'] and vals['expiry_date']:
		# 		wk_plan = self.pool.get('weekly.plans').search(cr,uid,[('cust','=',rec.customer.id)])
		# 		if wk_plan: self.pool.get('weekly.plans').write(cr,uid,wk_plan,{'current_date':vals['current_date'],'expiry_date':vals['expiry_date']})
		
		if 'paid_amount' in vals:
			if vals['paid_amount'] != rec.plan_price :
				raise Warning(_('Warning!!'), _('Paid Amount does not match plan price.'))
			# else : return super(Meal_plans, self).write(cr,uid,ids,vals,context=context)
		
		
		return super(Meal_plans, self).write(cr,uid,ids,vals,context=context)

	def unlink(self, cr, uid, ids, context=None):
		for obj in self.browse(cr, uid,ids, context):
			if obj.state in ('active'):
				raise Warning(_('Warning!!'), _('You cannot delete a meal plan which is already in Active state!!'))
		return super(Meal_plans, self).unlink(cr, uid, ids, context)


	@api.onchange('current_date','no_of_weeks','days_per_week')
	def _onchange_start_date(self):
		if self.current_date:
			weekday = datetime.strptime(self.current_date, '%Y-%m-%d').weekday()
			if weekday in [3,4]:
				raise osv.except_osv(_('Warning!'), _('Day should not Thursday or Friday!!'))

			current_date = datetime.strptime(self.current_date, '%Y-%m-%d').date()
			if not self.pause_date and not self.resume_date:
				self.expiry_date = date_by_adding_business_days(current_date,
																((self.no_of_weeks * self.days_per_week) - 1))

			else:
				pause_date = datetime.strptime(self.pause_date, '%Y-%m-%d').date()
				resume_date = datetime.strptime(self.resume_date, '%Y-%m-%d').date()
				paused_days = (resume_date - pause_date).days
				paused_days_counter = 0
				for i in range(paused_days + 1):
					each = pause_date + timedelta(days=i)
					if each.weekday() not in [4, 5]:
						paused_days_counter += 1
				prev_expiry_date = date_by_adding_business_days(current_date, ((self.no_of_weeks*self.days_per_week)-1))
				self.expiry_date = date_by_adding_business_days(prev_expiry_date, (paused_days_counter))

	def renewing_meal_plan(self, cr, uid, ids, context=None):
		return {
			'name' : _('Renew Meal Plan'),
			'view_type' : 'form',
			'view_mode' : 'form',
			'res_model' : 'renew.meal',
			'type' : 'ir.actions.act_window',
			'target' : 'new',
		}


	def create(self, cr, uid, vals, context=None):

		customer_id = vals.get('customer', None)
		start_date = vals.get('current_date', None)

		if vals.get('no_of_weeks'):
			weeks_total_cont = vals.get('no_of_weeks')
		else:
			weeks_total_cont = 4

		if vals.get('days_per_week'):
			days_per_week = vals.get('days_per_week')
		else:
			days_per_week = 5

		user_id = self.pool.get('res.users').search(cr, uid, [('partner_id','=', customer_id)])

		if (user_id and not self.pool.get('res.users').browse(cr, uid, user_id[0]).existing_customer):
		
			queued_plans = self.search(cr, uid,[('current_date','>',datetime.now().date())])
			all_current_dates =  self.read(cr, uid, queued_plans, ['current_date'])

			if all_current_dates:
				all_dates = []
				for i in all_current_dates:
					all_dates.append(i['current_date'])

				counter = collections.Counter(all_dates) # extracting counter of duplicated dates
				max_date = max(all_dates)
				weekday = datetime.strptime(max_date, '%Y-%m-%d').strftime("%A") # getting weekday
				for k,v in counter.items():
					if k == max_date:
						if v < 6:
							sat = datetime.strptime(k, '%Y-%m-%d')
						else:
							weekday = 'Saturday'
							sat = get_next_weekday(k, weekday)
				vals.update({'current_date': sat})

			else:
				date = datetime.now().date().strftime('%Y-%m-%d')
				sat = get_next_weekday(date, 'Saturday')
				vals.update({'current_date': sat})

			# expiry_date = date_by_adding_business_days(sat.date(), ((weeks_total_cont*days_per_week)-1))
		
		# if start_date:
		# 	current_date = datetime.strptime(start_date, '%Y-%m-%d')
		# 	expiry_date = date_by_adding_business_days(current_date.date(), ((weeks_total_cont*days_per_week)-1))
		# 	vals.update({'expiry_date': expiry_date})

		vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'meal_plan')
		
		# if vals['meals_per_day'] != vals['breakfast']+vals['mid']+vals['evening']+vals['night']:
		# 	raise osv.except_osv(_('Warning!'), _('Current Selection does not match with the No. of meals!!'))
		rec = self.search(cr, uid, [('customer', '=', vals['customer'])])
		if rec:
			raise osv.except_osv(_('Sorry!'),_('The customer has already plan.'))

		if self.search(cr, uid, [('customer', '=', vals['customer']),('state', 'in', ['active','draft','paused'])], context=context):
			raise osv.except_osv(_('Warning!'), _('Meal plan for this customer already exist!!'))
		else:
			return super(Meal_plans, self).create(cr, uid, vals, context=context)
						
	#buttons functionality while click buttom
	# def active_progressbar(self, cr, uid, ids, context=None):

	# 	vals = {'state': 'active'}
	# 	obj = self.browse(cr, uid, ids[0])
		
	# 	if not obj.breakfast and not obj.mid and not obj.evening and not obj.night and obj.meals_per_day != 4:
	# 		raise osv.except_osv(_('Error!'), _('Please choose Breakfast, MID, Evening, Night options!!'))

	# 	self.pool.get('res.partner').write(cr, uid, obj.customer.id, {'state': 'active'})

	# 	queued_plans = self.search(cr, uid,[('current_date','>',datetime.now().date())])
	# 	all_current_dates =  self.read(cr, uid, queued_plans, ['current_date'])

	# 	if all_current_dates:
	# 		all_dates = []
	# 		for i in all_current_dates:
	# 			all_dates.append(i['current_date'])

	# 		counter = collections.Counter(all_dates) # extracting counter of duplicated dates
	# 		max_date = max(all_dates)
	# 		weekday = datetime.strptime(max_date, '%Y-%m-%d').strftime("%A") # getting weekday
	# 		for k,v in counter.items():
	# 			if k == max_date:
	# 				if v < 3:
	# 					sat = datetime.strptime(k, '%Y-%m-%d')
	# 				else:
	# 					# assiging next weekday
	# 					if weekday == 'Saturday':
	# 						weekday = 'Sunday'
	# 					else:
	# 						weekday = 'Saturday'
	# 					sat = get_next_weekday(k, weekday)

	# 		vals.update({'current_date': sat})

	# 	else:
	# 		date = datetime.now().date().strftime('%Y-%m-%d')
	# 		sat = get_next_weekday(date, 'Saturday')
	# 		vals.update({'current_date': sat})


	# 	weeks_total_cont = obj.no_of_weeks
	# 	days_per_week = obj.days_per_week

	# 	expiry_date = date_by_adding_business_days(sat.date(), ((weeks_total_cont*days_per_week)-1))
	# 	vals.update({'expiry_date': expiry_date})
	# 	return self.write(cr, uid, ids, vals)

	def send_renew_link(self, cr, uid, ids, context=None):
		obj = self.browse(cr, uid, ids[0])
		if obj.state != 'cancelled':
			url = 'http://plan.athleat.ae/customer/renewal-meal-notify/'
			try:
				data = urllib.urlencode({
										"cid": obj.customer.id,
										# "cid": 165,
										})
				req = urllib2.Request(url,data=data,headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'})
				response = urllib2.urlopen(req)			
				self.write(cr, uid, ids, {'state': 'renewal-request-sent'})
			except Exception as e:
				print e
				raise osv.except_osv(_('Error!'),_('Unable to send email.'))
		return True

	def active_progressbar(self, cr, uid, ids, context=None):

		weeks_total_cont, days_per_week=0, 0
		vals = {'state': 'active'}
		obj = self.browse(cr, uid, ids[0])
		
		if obj.state == 'draft':
			url = 'http://plan.athleat.ae/customer/welcome-athleat-notify/'
			try:
				data = urllib.urlencode({
										"cid": obj.customer.id,
										# "cid": 165,
										})
				req = urllib2.Request(url,data=data,headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'})
				response = urllib2.urlopen(req)
			
			except Exception as e:
				print e
				raise osv.except_osv(_('Error!'),_('Unable to send email.'))

		if int(obj.breakfast) + int(obj.mid) + int(obj.evening) + int(obj.night) != obj.meals_per_day:
			raise osv.except_osv(_('Warning!'), _('Current Selection does not match with the No. of meals!!'))
		
		if not obj.breakfast and not obj.mid and not obj.evening and not obj.night and obj.meals_per_day != 4:
			raise osv.except_osv(_('Error!'), _('Please choose Breakfast, MID, Evening, Night options!!'))

		# self.pool.get('res.partner').write(cr, uid, obj.customer.id, {'state': 'active'})

		user_id = self.pool.get('res.users').search(cr, uid, [('partner_id','=', obj.customer.id)])

		if user_id and not self.pool.get('res.users').browse(cr, uid, user_id[0]).existing_customer :

			queued_plans = self.search(cr, uid,[('current_date','>',datetime.now().date())])
			all_current_dates =  self.read(cr, uid, queued_plans, ['current_date'])

			if all_current_dates:
				all_dates = []
				for i in all_current_dates:
					all_dates.append(i['current_date'])

				counter = collections.Counter(all_dates) # extracting counter of duplicated dates
				max_date = max(all_dates)
				weekday = datetime.strptime(max_date, '%Y-%m-%d').strftime("%A") # getting weekday
				for k,v in counter.items():
					if k == max_date:
						if v < 6:
							sat = datetime.strptime(k, '%Y-%m-%d')
						else:
							# assiging next weekday
							weekday = 'Saturday'
							sat = get_next_weekday(k, weekday)

				vals.update({'current_date': str(sat.date())})

			else:
				date = datetime.now().date().strftime('%Y-%m-%d')
				sat = get_next_weekday(date, 'Saturday')
				vals.update({'current_date': str(sat.date())})


			weeks_total_cont = obj.no_of_weeks
			days_per_week = obj.days_per_week

			start_date = datetime.strptime(obj.current_date, '%Y-%m-%d')

			expiry_date = date_by_adding_business_days(start_date.date(), ((weeks_total_cont*days_per_week)-1))
			vals.update({'expiry_date': expiry_date})
			self.specifically_select_meals_weekly(cr,uid,ids)#to run importing meal plans scheduler only for the activated meal plan
		return self.write(cr, uid, ids, vals)
		

	def resume_paused_progressbar(self, cr, uid, ids, context=None):
		return {
			'name': 'Pause/Resume Meal Plan',
			"view_mode": 'form',
			'res_model': 'pause.resume.meal',
			'type' : 'ir.actions.act_window',
			'target' : 'new',
		}

	def cancelled_progressbar(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'state': 'cancelled'})
	def draft_progressbar(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'state': 'draft'})
	def button_dummy(self, cr, uid, ids, context=None):
		return True

	def paused_progressbar(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'state': 'paused'})

	def calculate_days_left(self, cr, uid, context=None):
		
		for meal in self.pool.get('weekly.plans').search(cr, uid, [('state','=','confirm')], context=context):
			count=0
			wk_pl=self.pool.get('weekly.plans').browse(cr,uid,meal,context=context)
			meals_meals = wk_pl.meals_id1.ids+wk_pl.meals_id2.ids+wk_pl.meals_id3.ids+wk_pl.meals_id4.ids+wk_pl.meals_id5.ids+wk_pl.meals_id6.ids+wk_pl.meals_id7.ids+wk_pl.meals_id8.ids+wk_pl.meals_id9.ids+wk_pl.meals_id10.ids+wk_pl.meals_id11.ids+wk_pl.meals_id12.ids
			cust = self.pool.get('weekly.plans').browse(cr,uid,meal,context=context).cust.id
			for each in meals_meals:
				
				rec_date = self.pool.get('meal.meal').browse(cr,uid,each,context=context).date
				cur_date = datetime.today()
				if datetime.strptime(rec_date, '%Y-%m-%d') > datetime.strptime(time.strftime("%d-%m-%Y"), '%d-%m-%Y'):
					count += 1
			exists= self.search(cr,uid,[('customer','=',cust)])
			if exists:
				self.write(cr,uid,exists, {'days_left':count})
		return True

	def meal_plan_expiry_reminder(self,cr,uid,context=None):
		current_day = datetime.strftime(datetime.now().date() , '%Y-%m-%d')
		# third_day = datetime.strftime(datetime.now().date() + timedelta(days=3), '%Y-%m-%d')
		# meal_plans = self.search(cr,uid,[('expiry_date','=',third_day)])
		expired_meal_plans = self.search(cr,uid,[('expiry_date','<',current_day)])
		
		# template = self.pool.get('ir.model.data').get_object(cr, uid, 'athleat', 'notify_meal_plans_expiry')
		# values={}

		# if meal_plans:
		# 	for i in meal_plans:
		# 		cust_email = self.browse(cr,uid,i).customer.email
		# 		cust_name = self.browse(cr,uid,i).customer.name
				
		# 		email_template_obj = self.pool.get('email.template')
		# 		values = email_template_obj.generate_email_batch(cr, uid, template.id, [uid], context=context)
		# 		values['subject'] = 'Meal Plan Expiry' 
		# 		values['email_to'] = cust_email
		# 		values['email_from'] = 'plan@athleat.ae'
		# 		values['body_html'] = "<p> Dear %s,</p><p>Your Meal plan is about to expire in 3 days. You can either review your plan or cancel it.</p><p>Regards,</p><p>Team Athleat.</p>"%cust_name
		# 		values['body'] = 'body_html',
		# 		values['res_id'] = False
		# 		mail_mail_obj = self.pool.get('mail.mail')
		# 		x = uid
		# 		values[str(x)] = values.pop(uid)
		# 		msg_id = mail_mail_obj.create(cr,uid,values)
				# mail_mail_obj.send(cr, uid, [msg_id], context=context) 

		if expired_meal_plans:
			for y in expired_meal_plans:
				self.write(cr, uid, y, {'state': 'expired'})

		return True

	def show_meals_function(self,cr,uid,ids,context=None):
		data_obj = self.pool.get('ir.model.data')
		rec= self.browse(cr,uid,ids)
		
		tree_data_id = data_obj._get_id(cr, uid, 'athleat', 'meals_tree_view')
		if tree_data_id:
			tree_view_id = data_obj.browse(cr, uid, tree_data_id, context=context).res_id	

		form_data_id = data_obj._get_id(cr, uid, 'athleat', 'meals_form_view')	
		if form_data_id:
			form_view_id = data_obj.browse(cr, uid, form_data_id, context=context).res_id		

		current_date = rec.current_date
		expiry_date = rec.expiry_date
		cust_id = rec.customer.id

		records = self.pool.get('meal.meal').search(cr,uid,[('cust','=',cust_id),('date', '>=', current_date), ('date', '<=', expiry_date)])
		
		return {
			'name' : _('Daily Meals'),
			'view_type' : 'form',
			'res_model' : 'meal.meal',
			'view_id' : False,
			'domain' :"[('id', 'in',%s)]" %(records),
			'views' : [(tree_view_id, 'tree'),(form_view_id, 'form')],
			'type' : 'ir.actions.act_window',
			'target' : 'current',
			'nodestroy' : True,
			'context' : "{'search_default_status': 1}",
		}

	


	def meals_selection_weekly(self,cr,uid,context=None):
		ml_dict ={}
		items=[]
		week1_items =week2_items=week3_items=week4_items=[]
		list_items=[]
		alst = blst = clst = dlst = []

		master_obj = self.pool.get('master.plans')
		meal_obj = self.pool.get('meal.meal')
		meal_plan_obj = self.pool.get('meal.plans')

		meal_plans = meal_plan_obj.search(cr,uid,[('current_date','!=',False)])
		if meal_plans:
			for i in meal_plans:
				master, item_list = [], []
			
				rec=meal_plan_obj.browse(cr,uid,i)
				
				k = rec.current_date
				date =  datetime.strptime(k, '%Y-%m-%d')
				week = (date.day-1)//7
				month = date.strftime("%b").lower()
				weeks = rec.no_of_weeks

				meal_plan_type, veg_type, meals_count, days_count = rec.meal_plan_type, rec.customer.meal_type, rec.meals_per_day, rec.days_per_week
			
				disliked, liked, substitute = [], [], rec.substitutes.ids
				
				breakfast, mid, evening, night = rec.breakfast, rec.mid, rec.evening, rec.night

				if meals_count == breakfast+mid+evening+night:
					next_mon1 = (date+timedelta(weeks=4)).strftime("%b").lower()
					next_mon2 = (date+timedelta(weeks=8)).strftime("%b").lower()

					if weeks in [1,2,4]: master += master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',month)])
					elif weeks == 8: 
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',month)])]
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',next_mon1)])]
					elif weeks == 12: 
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',month)])]
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',next_mon1)])]
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',next_mon2)])]
					else: pass

					for ms in master:
						carbs = master_obj.browse(cr,uid,ms).meal_plan_type
						types = master_obj.browse(cr,uid,ms).meal_type
						week1_meals = [master_obj.browse(cr,uid,ms).week1_meals_id.ids]
						week2_meals = [master_obj.browse(cr,uid,ms).week2_meals_id.ids]
						week3_meals = [master_obj.browse(cr,uid,ms).week3_meals_id.ids]
						week4_meals = [master_obj.browse(cr,uid,ms).week4_meals_id.ids]
						
						if weeks == 1 : wk_needed = [week1_meals]
						elif weeks == 2: wk_needed = [week1_meals, week2_meals]
						else: wk_needed = [week1_meals, week2_meals, week3_meals, week4_meals]
					
						for weekmeal in wk_needed:
							
							master_list=[]

							if weekmeal:
								meals = weekmeal[0]

								if meals:
									for each in meals:

										master_rec = meal_obj.browse(cr,uid,each)
										ml_dict = {'day':master_rec.day,'carb':master_rec.carb,'price':master_rec.price,'fat':master_rec.fat,'protein':master_rec.protein,'quantity':master_rec.quantity,'calories':master_rec.calories}
									
										if breakfast and master_rec.item1.id: ml_dict.update({'item1':master_rec.item1.id})
										else:ml_dict.update({'item1':False})
										if mid and master_rec.item2.id:  ml_dict.update({'item2':master_rec.item2.id})
										else:ml_dict.update({'item2':False})
										if evening and master_rec.item3.id:  ml_dict.update({'item3':master_rec.item3.id})
										else:ml_dict.update({'item3':False})
										if night and master_rec.item4.id:  ml_dict.update({'item4':master_rec.item4.id})
										else:ml_dict.update({'item4':False})
										master_list+=[ml_dict]
								else:
									for inn in xrange(days_count):
										ml_dict = {'day':inn+1,'carb':0,'price':0,'fat':0,'protein':0,'quantity':0,'calories':0,'item1':False,'item2':False,'item3':False,'item4':False}
										
										if breakfast : ml_dict.update({'item1':False})
										if mid :  ml_dict.update({'item2':False})
										if evening :  ml_dict.update({'item3':False})
										if night :  ml_dict.update({'item4':False})
										master_list+=[ml_dict]

							if carbs == 'high carb' and types == 'veg': item_list += [master_list]
							elif carbs == 'high carb' and types == 'nonveg': item_list += [master_list]
							elif carbs == 'low carb' and types == 'veg': item_list += [master_list]
							elif carbs == 'low carb' and types == 'nonveg': item_list += [master_list]
							else: pass

					index=0
					lame_list=[]

					for meels in item_list:
						meal_id=[]
						if meels:
							
							for ir in meels:
								if 'item1' in ir: lame_list += [ir['item1']]
								if 'item2' in ir: lame_list += [ir['item2']]
								if 'item3' in ir: lame_list += [ir['item3']]
								if 'item4' in ir: lame_list += [ir['item4']]
								if disliked:
									for dislike in disliked:
										if 'item1' in ir and dislike == ir['item1'] and substitute:ir['item1']=random.sample(substitute,1)[0]
										if 'item2' in ir and dislike == ir['item2'] and substitute:ir['item2']=random.sample(substitute,1)[0]
										if 'item3' in ir and dislike == ir['item3'] and substitute:ir['item3']=random.sample(substitute,1)[0]
										if 'item4' in ir and dislike == ir['item4'] and substitute:ir['item4']=random.sample(substitute,1)[0]
								if liked:
									for like in liked:
										if like in lame_list:pass
										else: ir['item2']=random.sample(liked,1)[0]
								
								if len(meal_id) != days_count:
									count_dict = {'day':ir['day'],'calories':ir['calories'],'fat':ir['fat'],'carb':ir['carb'],'protein':ir['protein'],'price':ir['price'],'quantity':ir['quantity']}
								
									if 'item1' in ir: count_dict.update({'item1':ir['item1']})
									if 'item2' in ir: count_dict.update({'item2':ir['item2']})
									if 'item3' in ir: count_dict.update({'item3':ir['item3']})
									if 'item4' in ir: count_dict.update({'item4':ir['item4']})
									meal_id = meal_id + [(0,0,count_dict)]
						# for creating records with empty items, incase meals are not defined in master plans
						else:
							for ir in xrange(days_count):
								meal_id = meal_id + [(0,0,{'item1':[],'item2':[],'item3':[],'item4':[],'day':ir+1})]

						if index == 0 and not rec.week1_meal_plan.ids : self.write(cr,uid,i,{'week1_meal_plan':meal_id})
						elif index == 1 and not rec.week2_meal_plan.ids : self.write(cr,uid,i,{'week2_meal_plan':meal_id})
						elif index == 2 and not rec.week3_meal_plan.ids : self.write(cr,uid,i,{'week3_meal_plan':meal_id})
						elif index == 3 and not rec.week4_meal_plan.ids : self.write(cr,uid,i,{'week4_meal_plan':meal_id})
						elif index == 4 and not rec.week5_meal_plan.ids : self.write(cr,uid,i,{'week5_meal_plan':meal_id})
						elif index == 5 and not rec.week6_meal_plan.ids : self.write(cr,uid,i,{'week6_meal_plan':meal_id})
						elif index == 6 and not rec.week7_meal_plan.ids : self.write(cr,uid,i,{'week7_meal_plan':meal_id})
						elif index == 7 and not rec.week8_meal_plan.ids : self.write(cr,uid,i,{'week8_meal_plan':meal_id})
						elif index == 8 and not rec.week9_meal_plan.ids : self.write(cr,uid,i,{'week9_meal_plan':meal_id})
						elif index == 9 and not rec.week10_meal_plan.ids : self.write(cr,uid,i,{'week10_meal_plan':meal_id})
						elif index == 10 and not rec.week11_meal_plan.ids : self.write(cr,uid,i,{'week11_meal_plan':meal_id})
						elif index == 11 and not rec.week12_meal_plan.ids : self.write(cr,uid,i,{'week12_meal_plan':meal_id})
						else: pass

						if index == 0 and rec.week1_meal_plan.ids : 
							for day in rec.week1_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week1_meal_plan.ids).index(day)][2])
						elif index == 1 and rec.week2_meal_plan.ids : 
							for day in rec.week2_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week2_meal_plan.ids).index(day)][2])
						elif index == 2 and rec.week3_meal_plan.ids : 
							for day in rec.week3_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week3_meal_plan.ids).index(day)][2])
						elif index == 3 and rec.week4_meal_plan.ids : 
							for day in rec.week4_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week4_meal_plan.ids).index(day)][2])
						elif index == 4 and rec.week5_meal_plan.ids : 
							for day in rec.week5_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week5_meal_plan.ids).index(day)][2])
						elif index == 5 and rec.week6_meal_plan.ids : 
							for day in rec.week6_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week6_meal_plan.ids).index(day)][2])
						elif index == 6 and rec.week7_meal_plan.ids : 
							for day in rec.week7_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week7_meal_plan.ids).index(day)][2])
						elif index == 7 and rec.week8_meal_plan.ids : 
							for day in rec.week8_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week8_meal_plan.ids).index(day)][2])
						elif index == 8 and rec.week9_meal_plan.ids : 
							for day in rec.week9_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week9_meal_plan.ids).index(day)][2])
						elif index == 9 and rec.week10_meal_plan.ids :
							for day in rec.week10_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week10_meal_plan.ids).index(day)][2])
						elif index == 10 and rec.week11_meal_plan.ids :
							for day in rec.week11_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week11_meal_plan.ids).index(day)][2])
						elif index == 11 and rec.week12_meal_plan.ids :
							for day in rec.week12_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week12_meal_plan.ids).index(day)][2])
						else: pass
						index+=1

	def pause_meal_plans(self,cr,uid,context=None):
		meal_plans = self.search(cr,uid,[('pause_date','=',datetime.now().date())])
		if meal_plans: self.write(cr,uid,meal_plans,{'state':'paused'})

		resuming_meal_plans = self.search(cr,uid,[('resume_date','=',datetime.now().date())])
		if resuming_meal_plans: self.write(cr,uid,resuming_meal_plans,{'state':'active'})
		return True


	def specifically_select_meals_weekly(self,cr,uid,ids,context=None):
		ml_dict ={}
		items=[]
		week1_items =week2_items=week3_items=week4_items=[]
		list_items=[]
		alst = blst = clst = dlst = []

		master_obj = self.pool.get('master.plans')
		meal_obj = self.pool.get('meal.meal')
		meal_plan_obj = self.pool.get('meal.plans')

		meal_plans = meal_plan_obj.search(cr,uid,[('current_date','!=',False)])
		if meal_plans:
			for i in meal_plans:
				master, item_list = [], []
			
				rec=meal_plan_obj.browse(cr,uid,i)
				
				k = rec.current_date
				date =  datetime.strptime(k, '%Y-%m-%d')
				week = (date.day-1)//7
				month = date.strftime("%b").lower()
				weeks = rec.no_of_weeks

				meal_plan_type, veg_type, meals_count, days_count = rec.meal_plan_type, rec.customer.meal_type, rec.meals_per_day, rec.days_per_week
			
				disliked, liked, substitute = [], [], rec.substitutes.ids
				
				breakfast, mid, evening, night = rec.breakfast, rec.mid, rec.evening, rec.night

				if meals_count == breakfast+mid+evening+night:
					next_mon1 = (date+timedelta(weeks=4)).strftime("%b").lower()
					next_mon2 = (date+timedelta(weeks=8)).strftime("%b").lower()

					if weeks in [1,2,4]: master += master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',month)])
					elif weeks == 8: 
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',month)])]
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',next_mon1)])]
					elif weeks == 12: 
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',month)])]
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',next_mon1)])]
						master += [master_obj.search(cr,uid,[('meal_plan_type','=',meal_plan_type),('meal_type','=',veg_type),('month','=',next_mon2)])]
					else: pass

					for ms in master:
						carbs = master_obj.browse(cr,uid,ms).meal_plan_type
						types = master_obj.browse(cr,uid,ms).meal_type
						week1_meals = [master_obj.browse(cr,uid,ms).week1_meals_id.ids]
						week2_meals = [master_obj.browse(cr,uid,ms).week2_meals_id.ids]
						week3_meals = [master_obj.browse(cr,uid,ms).week3_meals_id.ids]
						week4_meals = [master_obj.browse(cr,uid,ms).week4_meals_id.ids]
						
						if weeks == 1 : wk_needed = [week1_meals]
						elif weeks == 2: wk_needed = [week1_meals, week2_meals]
						else: wk_needed = [week1_meals, week2_meals, week3_meals, week4_meals]

						for weekmeal in wk_needed:
							
							master_list=[]

							if weekmeal:
								meals = weekmeal[0]

								if meals:
									for each in meals:

										master_rec = meal_obj.browse(cr,uid,each)
										ml_dict = {'day':master_rec.day,'carb':master_rec.carb,'price':master_rec.price,'fat':master_rec.fat,'protein':master_rec.protein,'quantity':master_rec.quantity,'calories':master_rec.calories}
									
										if breakfast and master_rec.item1.id: ml_dict.update({'item1':master_rec.item1.id})
										else:ml_dict.update({'item1':False})
										if mid and master_rec.item2.id:  ml_dict.update({'item2':master_rec.item2.id})
										else:ml_dict.update({'item2':False})
										if evening and master_rec.item3.id:  ml_dict.update({'item3':master_rec.item3.id})
										else:ml_dict.update({'item3':False})
										if night and master_rec.item4.id:  ml_dict.update({'item4':master_rec.item4.id})
										else:ml_dict.update({'item4':False})
										master_list+=[ml_dict]
								else:
									for inn in xrange(days_count):
										ml_dict = {'day':inn+1,'carb':0,'price':0,'fat':0,'protein':0,'quantity':0,'calories':0,'item1':False,'item2':False,'item3':False,'item4':False}
										
										if breakfast : ml_dict.update({'item1':False})
										if mid :  ml_dict.update({'item2':False})
										if evening :  ml_dict.update({'item3':False})
										if night :  ml_dict.update({'item4':False})
										master_list+=[ml_dict]

							if carbs == 'high carb' and types == 'veg': item_list += [master_list]
							elif carbs == 'high carb' and types == 'nonveg': item_list += [master_list]
							elif carbs == 'low carb' and types == 'veg': item_list += [master_list]
							elif carbs == 'low carb' and types == 'nonveg': item_list += [master_list]
							else: pass

					index=0
					lame_list=[]
					for meels in item_list:
						meal_id=[]
						if meels:
							
							for ir in meels:
								if 'item1' in ir: lame_list += [ir['item1']]
								if 'item2' in ir: lame_list += [ir['item2']]
								if 'item3' in ir: lame_list += [ir['item3']]
								if 'item4' in ir: lame_list += [ir['item4']]
								if disliked:
									for dislike in disliked:
										if 'item1' in ir and dislike == ir['item1'] and substitute:ir['item1']=random.sample(substitute,1)[0]
										if 'item2' in ir and dislike == ir['item2'] and substitute:ir['item2']=random.sample(substitute,1)[0]
										if 'item3' in ir and dislike == ir['item3'] and substitute:ir['item3']=random.sample(substitute,1)[0]
										if 'item4' in ir and dislike == ir['item4'] and substitute:ir['item4']=random.sample(substitute,1)[0]
								if liked:
									for like in liked:
										if like in lame_list:pass
										else: ir['item2']=random.sample(liked,1)[0]
								
								if len(meal_id) != days_count:
									count_dict = {'day':ir['day'],'calories':ir['calories'],'fat':ir['fat'],'carb':ir['carb'],'protein':ir['protein'],'price':ir['price'],'quantity':ir['quantity']}
								
									if 'item1' in ir: count_dict.update({'item1':ir['item1']})
									if 'item2' in ir: count_dict.update({'item2':ir['item2']})
									if 'item3' in ir: count_dict.update({'item3':ir['item3']})
									if 'item4' in ir: count_dict.update({'item4':ir['item4']})
									meal_id = meal_id + [(0,0,count_dict)]
						# for creating records with empty items, incase meals are not defined in master plans
						else:
							for ir in xrange(days_count):
								meal_id = meal_id + [(0,0,{'item1':[],'item2':[],'item3':[],'item4':[],'day':ir+1})]

						if index == 0 and not rec.week1_meal_plan.ids : self.write(cr,uid,i,{'week1_meal_plan':meal_id})
						elif index == 1 and not rec.week2_meal_plan.ids : self.write(cr,uid,i,{'week2_meal_plan':meal_id})
						elif index == 2 and not rec.week3_meal_plan.ids : self.write(cr,uid,i,{'week3_meal_plan':meal_id})
						elif index == 3 and not rec.week4_meal_plan.ids : self.write(cr,uid,i,{'week4_meal_plan':meal_id})
						elif index == 4 and not rec.week5_meal_plan.ids : self.write(cr,uid,i,{'week5_meal_plan':meal_id})
						elif index == 5 and not rec.week6_meal_plan.ids : self.write(cr,uid,i,{'week6_meal_plan':meal_id})
						elif index == 6 and not rec.week7_meal_plan.ids : self.write(cr,uid,i,{'week7_meal_plan':meal_id})
						elif index == 7 and not rec.week8_meal_plan.ids : self.write(cr,uid,i,{'week8_meal_plan':meal_id})
						elif index == 8 and not rec.week9_meal_plan.ids : self.write(cr,uid,i,{'week9_meal_plan':meal_id})
						elif index == 9 and not rec.week10_meal_plan.ids : self.write(cr,uid,i,{'week10_meal_plan':meal_id})
						elif index == 10 and not rec.week11_meal_plan.ids : self.write(cr,uid,i,{'week11_meal_plan':meal_id})
						elif index == 11 and not rec.week12_meal_plan.ids : self.write(cr,uid,i,{'week12_meal_plan':meal_id})
						else: pass

						if index == 0 and rec.week1_meal_plan.ids : 
							for day in rec.week1_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week1_meal_plan.ids).index(day)][2])
						elif index == 1 and rec.week2_meal_plan.ids : 
							for day in rec.week2_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week2_meal_plan.ids).index(day)][2])
						elif index == 2 and rec.week3_meal_plan.ids : 
							for day in rec.week3_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week3_meal_plan.ids).index(day)][2])
						elif index == 3 and rec.week4_meal_plan.ids : 
							for day in rec.week4_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week4_meal_plan.ids).index(day)][2])
						elif index == 4 and rec.week5_meal_plan.ids : 
							for day in rec.week5_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week5_meal_plan.ids).index(day)][2])
						elif index == 5 and rec.week6_meal_plan.ids : 
							for day in rec.week6_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week6_meal_plan.ids).index(day)][2])
						elif index == 6 and rec.week7_meal_plan.ids : 
							for day in rec.week7_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week7_meal_plan.ids).index(day)][2])
						elif index == 7 and rec.week8_meal_plan.ids : 
							for day in rec.week8_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week8_meal_plan.ids).index(day)][2])
						elif index == 8 and rec.week9_meal_plan.ids : 
							for day in rec.week9_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week9_meal_plan.ids).index(day)][2])
						elif index == 9 and rec.week10_meal_plan.ids :
							for day in rec.week10_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week10_meal_plan.ids).index(day)][2])
						elif index == 10 and rec.week11_meal_plan.ids :
							for day in rec.week11_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week11_meal_plan.ids).index(day)][2])
						elif index == 11 and rec.week12_meal_plan.ids :
							for day in rec.week12_meal_plan.ids: meal_obj.write(cr,uid,day,meal_id[(rec.week12_meal_plan.ids).index(day)][2])
						else: pass
						index+=1
Meal_plans()



