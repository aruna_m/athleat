from openerp.osv import osv,fields

class Sauce(osv.osv):
	_name="sauce.menu"
	_rec_name="name"
	_columns={
	'name':fields.char('Name'),
	'carbs':fields.float('Carbs'),
	'protein':fields.float('Protein'),
	'fat':fields.float('Fat'),
	'price':fields.float('Price'),
	'servings':fields.integer('Servings'),
	}

Sauce()