from openerp.osv import osv, fields
import urllib
import urllib2
from openerp import api
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools.translate import _


class Request_payments(osv.osv):

	_name = "request.payments"

	_rec_name = "customer_id"

	_order = 'id desc'

	_inherit = ['mail.thread']

	_columns = {
		'customer_id': fields.many2one('res.partner', 'Customer Name'),
		'email': fields.char('Email', track_visibility='onchange'),
		'invoice_no': fields.char('Invoice Number', track_visibility='onchange'),
		'amount': fields.float('Amount', track_visibility='onchange'),
		'items': fields.text('Item Details', track_visibility='onchange'),
		'description': fields.text('Description', track_visibility='onchange'),
		'merchant_reference': fields.char('Merchant Reference', track_visibility='onchange'),
		'payfort_unique_id': fields.char('Payfort ID', track_visibility='onchange'),
		'status': fields.selection([('draft', 'Draft'),
									('requested', 'Requested'),
									('pending', 'Pending'),
									('payment_failed', 'Failed'),
									('received', 'Payment Received'), ], 'Status', track_visibility='onchange'),
	}
	_defaults = {
		'status': 'draft',
	}	


	@api.onchange('customer_id')
	def _onchange_customer_id(self):
		if self.customer_id.email:
			self.email = self.customer_id.email


	def create(self,cr,uid,vals,context=None):
		if vals['invoice_no']:
			if self.pool.get('request.payments').search(cr,uid,[('invoice_no','=',vals['invoice_no'])]):
				raise osv.except_osv(_('Error!'), _('Invoice No. must be unique!!'))
			
		return super(Request_payments, self).create(cr, uid, vals, context=context)

	def write(self,cr,uid,ids,vals,context=None):
		if 'invoice_no' in vals and vals['invoice_no']:
			if self.pool.get('request.payments').search(cr,uid,[('invoice_no','=',vals['invoice_no'])]):
				raise osv.except_osv(_('Error!'), _('Invoice No. must be unique!!'))
		return super(Request_payments,self).write(cr,uid,ids,vals,context=context)

	def unlink(self, cr, uid, ids, context=None):
		for obj in self.browse(cr, uid,ids, context):
			if obj.status in ('received'):
				raise Warning(_('Warning!!'), _('You cannot delete a Received Payment which is Paid!!'))
		return super(Request_payments, self).unlink(cr, uid, ids, context)

	def send_payment_request(self, cr, uid, ids, context=None):
		url = 'http://plan.athleat.ae/customer/payment_request/'
		pay_obj = self.browse(cr, uid, ids[0])
		try:
			data = urllib.urlencode({"customer_name": str(pay_obj.customer_id.name),
									 "customer_email": str(pay_obj.email),
									 "invoice_number": str(pay_obj.invoice_no),
									 "amount": int(pay_obj.amount),
									 "description": str(pay_obj.description),
									 "item_details": str(pay_obj.items),
									 })
			req = urllib2.Request(url, data=data, headers={
				'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'})
			response = urllib2.urlopen(req)
			self.write(cr, uid, ids, {'status': 'requested'})
		except Exception as e:
			print e
			raise osv.except_osv(_('Error!'), _(
				'Unable to send email.'))
		return True

	def payment_recieved_status(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'status': 'received'})

	def copy(self, cr, uid, id, default={}, context=None):
		pay_obj = self.browse(cr, uid, id)
		if not default:
			default = {}
		default.update({
			'merchant_reference': False,
			'payfort_unique_id': False,
			'status': 'draft',
			'invoice_no': '',
			'amount': int(pay_obj.amount),
			'email': str(pay_obj.email),
			'items': str(pay_obj.items),
		})
		return super(Request_payments, self).copy(cr, uid, id, default, context=context)

Request_payments()
