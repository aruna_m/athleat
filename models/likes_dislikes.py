from openerp.osv import osv,fields

class Likes_Dislikes(osv.osv):
	_name="likes.dislikes"
	_rec_name="name"
	_columns={
	'name':fields.char('Name'),
	'type':fields.selection([('likes','Likes'),
							('dislikes','Dislikes'),],'Type'),
	}

Likes_Dislikes()