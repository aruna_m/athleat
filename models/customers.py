from openerp.osv import osv,fields
from datetime import datetime,timedelta

from openerp import api, SUPERUSER_ID
import HTML
from openerp import api
from openerp.tools.translate import _
import random
# import xlrd
# from xlrd import open_workbook
import os
from openerp.exceptions import except_orm, Warning
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT, ustr

def now(**kwargs):
	dt = datetime.now() + timedelta(**kwargs)
	return dt.strftime(DEFAULT_SERVER_DATETIME_FORMAT)


class Customer(osv.osv):
	
	_inherit='res.partner'

	def _get_scheduled_meals(self, cr, uid, ids, field_name, arg, context=None):
		res = {}
		d = datetime.now().date()
		scheduled_plans=[]
		#Convert date type from datetime.date type into string
		today = datetime.strftime(d, "%Y-%m-%d %H:%M:%S")

		for partner in self.browse(cr, uid, ids, context=context):
			
			scheduled_plan_id = self.pool.get('weekly.plans').search(cr, uid,[('cust','=',partner.id)])
			for wk in ['weekly_id1','weekly_id2','weekly_id3','weekly_id4','weekly_id5','weekly_id6','weekly_id7','weekly_id8','weekly_id9','weekly_id10','weekly_id11','weekly_id12']:
				scheduled_plans += self.pool.get('meal.meal').search(cr, uid,[(wk,'in',scheduled_plan_id),('date','>=',today)])
			res[partner.id] = len(scheduled_plans)
		return res
	
	def _get_meal_info(self, cr, uid, ids, field_name, arg, context=None):
		res = {}
		scheduled_items = [] 
		served_items = []
	
		for obj in self.browse(cr, uid, ids):

			res[obj.id] = { 'scheduled_items' : [], 'served_items' : [] }

			scheduled_plan = self.pool.get('weekly.plans').search(cr,uid,[('cust','=',obj.id)])
			meals_rec=self.pool.get('weekly.plans').browse(cr,uid,scheduled_plan)
			meals =  meals_rec.meals_id1.ids+meals_rec.meals_id2.ids+meals_rec.meals_id3.ids+meals_rec.meals_id4.ids+meals_rec.meals_id5.ids+meals_rec.meals_id6.ids+meals_rec.meals_id7.ids+meals_rec.meals_id8.ids+meals_rec.meals_id9.ids+meals_rec.meals_id10.ids+meals_rec.meals_id11.ids+meals_rec.meals_id12.ids
			for each in meals:
				status = self.pool.get('meal.meal').browse(cr,uid,each).status
				if status == 'scheduled':
					scheduled_items += [each]
				elif status == 'served':
					served_items += [each]
				else:
					pass
		
			res[obj.id]['scheduled_meals'] = scheduled_items
			res[obj.id]['served_meals'] = served_items

		return res

	_columns={

	'carb_type':fields.selection([('high carb','High Carb'),
								('low carb','Low Carb'),
								('customize','Customized')],'Carb Type'),
	'gender':fields.selection([('male','Male'),							
							('female','Female'),],"Gender"),
	'date_of_join':fields.date('Date of Join'),
	'contact_no':fields.char('Contact Number'),
	'villa_no': fields.char('Villa No'),
	'building_name': fields.char('Building Name'),
	'landmark': fields.char('Landmark'),
	# 'email':fields.char('Email'),
	# 'state': fields.selection([('new','New'),							
	# 						('queued','Queued'),
	# 						('active','Active'),
	# 						('in active','InActive')],"Status"),
	'meal_type': fields.selection([('veg','Veg'),
								('nonveg','Non Veg')],"Meal Preference"),
	'weight':fields.float('Weight'),
	'rel_id':fields.many2one('recipies.meal','Relation'),
	
	'liked_meals_ids':fields.many2many('recipies.meal','rel_liked_meals','rel_id','recipie_id','Liked Meals'),
	# 'disliked_meals_ids':fields.many2many('recipies.meal','rel_disiked_meals','rel_id','recipie_id','Disliked Meals/Allergies'),
	
	'liked_meals2_ids':fields.many2many('likes.dislikes','rel_likes_item_id','customer_id','item_id','Liked Meals'),
	'disliked_meals2_ids':fields.many2many('likes.dislikes','rel_exclusion_item_id','customer_id','item_id','Disliked Meals/Allergies'),
	
	'related_user':fields.many2one('res.users','Related User'),
	# 'user':fields.boolean('User'),
	'breakfast' : fields.boolean('Breakfast ?'),
	'scheduled_meal_count': fields.function(_get_scheduled_meals, type="integer", string="Scheduled Meals"),
	'served_meals': fields.function(_get_meal_info, type="one2many", object="res_partner2_id",relation="meal.meal",multi="sums"),
	'scheduled_meals': fields.function(_get_meal_info, type="one2many", object="res_partner1_id",relation="meal.meal",multi="sums"),
	}

	# _defaults = {
	# 'state': 'new',
	# }

	_sql_constraints = [
		('uniq_cust_details', 'unique (name,contact_no)',
		'Customer with given details already exists'),
		]

	# def create(self,cr,uid,vals,context=None):
	# 	vals['notify_email'] = 'always'
	# 	rec=super(Customer,self).create(cr,uid,vals,context=context)

	# 	values = {
	# 		 'active': True,
	# 		 'login': vals.get('email'),
	# 		 'partner_id': rec,
	# 		 'create_uid': 1,
	# 		 'write_uid': 1,
	# 		 'display_groups_suggestions': True,
	# 		 'share': True
	# 		}
	# 	# user_id = self.pool.get('res.users').create(cr,SUPERUSER_ID,values,context=context)
	# 	# self.write(cr,uid,rec,{'related_user':user_id})
	# 	return rec

	#buttons functionality while click button
	# def approve_button(self, cr, uid, ids, context=None):
	# 	return self.write(cr, uid, ids, {'state': 'queued'})
	# def active_button(self, cr, uid, ids, context=None):
	# 	return self.write(cr, uid, ids, {'state': 'active'})
	# def Deactive_button(self, cr, uid, ids, context=None):
	# 	return self.write(cr, uid, ids, {'state': 'in active'})	
	def send_daily_customersinfo_admin(self,cr,uid,context=None):
		template = self.pool.get('ir.model.data').get_object(cr, uid, 'athleat', 'notify_customers_admin')
		values={}
		
		admin_email = str(self.pool.get('res.users').browse(cr,uid,SUPERUSER_ID).email)
		table_data = HTML.Table(header_row=['Name','Carb Type','Meal Preference', 'Liked Meals','Disliked Meals'])

		customers = self.search(cr,uid,[('customer','=',True)])
		if customers:
			for i in customers:
				liked_items =''
				disliked_items =''

				name = self.browse(cr,uid,i).name
				meal_type = self.browse(cr,uid,i).meal_type
				carb_type = self.browse(cr,uid,i).carb_type
				liked = self.browse(cr,uid,i).liked_meals2_ids.ids
				
				if liked:
					for like in liked:
						liked_items += self.pool.get('likes.dislikes').browse(cr,uid,like).name
				# disliked = self.browse(cr,uid,i).disliked_meals_ids.ids
				
				# if disliked:
				# 	for dislike in disliked:
				# 		disliked_items += self.pool.get('recipies.meal').browse(cr,uid,dislike).name

				table_data.rows.append([str(name), str(carb_type), str(meal_type), str(liked_items),str(disliked_items)])

			email_template_obj = self.pool.get('email.template')
			values = email_template_obj.generate_email_batch(cr, uid, template.id, [uid], context=context)
			values['subject'] = 'Customer Details' 
			values['email_to'] = admin_email
			values['email_from'] = 'hello@athleat.ae'
			values['body_html'] = "Dear Admin,<div><br></div><div>Following are the details of Customers and their preferences :</div><div>%s\n</div><div><br></div><div>Regards,</div><div>Athleat Team.</div>"%table_data
			values['body'] = 'body_html',
			values['res_id'] = False
			mail_mail_obj = self.pool.get('mail.mail')
			x = uid
			values[str(x)] = values.pop(uid)
			msg_id = mail_mail_obj.create(cr,uid,values)
			mail_mail_obj.send(cr, uid, [msg_id], context=context) 
		
		return True

	def import_customers_excel(self,cr,uid,ids,context=None):

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		xl_workbook = xlrd.open_workbook(BASE_DIR +'/excel/EHAPI COMPLETE DETAILS OF ACTIVE CUSTOMERS.xlsx')
		sheet_names = xl_workbook.sheet_names()
		
		xl_sheet0 = xl_workbook.sheet_by_name(sheet_names[0])
		for row in range(1,xl_sheet0.nrows):
			if row<55:
				name = xl_sheet0.cell(row, 1).value
				phone = str(int(xl_sheet0.cell(row, 2).value))
				email = xl_sheet0.cell(row, 3).value
				carb_type = xl_sheet0.cell(row, 4).value
				no_of_meals = xl_sheet0.cell(row, 5).value
				bf = xl_sheet0.cell(row, 6).value
				addon = xl_sheet0.cell(row, 7).value
				

				customer_id = self.search(cr,uid,[('name','=',str(name))],context=context)
				if not customer_id:
					customer_id = self.create(cr,uid, {'name': str(name), 'carb_type': str(carb_type).lower().strip(),'contact_no': str(phone),'email': str(email), 'meal_type': 'nonveg'},context=context)

				else:
					customer_id = customer_id[0]
				# carb_type = self.browse(cr,uid,customer_id,context=context).carb_type
				vals = {
					'customer':customer_id,
					'meal_plan_type': str(carb_type).lower().strip(),
					'no_of_weeks':4,
				}

				if addon:
					addon_id = self.pool.get('addons.conf').search(cr, uid, [('name','ilike', str(addon))])
					if not addon_id:
						addon_id = self.pool.get('addons.conf').create(cr, uid, {'name': str(addon)})
					vals.update({'addons': [(6, 0, addon_id)]})
				if bf:
					vals.update({'breakfast': True})

				if int(no_of_meals) > 10:
					vals.update({'meals_per_day': 3})
				else:
					vals.update({'meals_per_day': int(no_of_meals)})

				self.pool.get('meal.plans').create(cr,uid,vals,context=context)
			else:
				pass
		return True


	def get_meal_plans(self,cr,uid,ids,context=None):
		cust_id = self.browse(cr,uid,ids).id
		meal_plans = self.pool.get('meal.plans').search(cr, uid, [('customer','=',cust_id)])
		models_data = self.pool.get('ir.model.data')
		meal_plans_tree = models_data._get_id(cr, uid, 'athleat', 'meal_plans_tree_view')
		return {
			'name': 'Meal Plans',
			'view_type': 'form',
			"view_mode": 'tree,form',
			'res_model': 'meal.plans',
			'type': 'ir.actions.act_window',
			'search_view_id': meal_plans_tree,
			'domain': "[('id', 'in',%s)]" % (meal_plans),
		}


	def get_scheduled_plans(self,cr,uid,ids,context=None):
		data_obj = self.pool.get('ir.model.data')
		rec= self.browse(cr,uid,ids)
		
		tree_data_id = data_obj._get_id(cr, uid, 'athleat', 'meals_tree_view')
		if tree_data_id:
			tree_view_id = data_obj.browse(cr, uid, tree_data_id, context=context).res_id	

		form_data_id = data_obj._get_id(cr, uid, 'athleat', 'meals_form_view')	
		if form_data_id:
			form_view_id = data_obj.browse(cr, uid, form_data_id, context=context).res_id		

		# current_date = rec.current_date
		# expiry_date = rec.expiry_date
		cust_id = rec.id

		records = self.pool.get('meal.meal').search(cr,uid,[('cust','=',cust_id),('status','in',['scheduled','served'])])
		
		return {
			'name': _('Total Meals'),
			'view_type': 'form',
			'res_model': 'meal.meal',
			'view_id': False,
			'domain':"[('id', 'in',%s)]" %(records),
			'views': [(tree_view_id, 'tree'),(form_view_id, 'form')],
			'type': 'ir.actions.act_window',
			'target': 'current',
			'nodestroy': True,
			'context' : "{'search_default_status': 1}",
		}

		# cust_id = self.browse(cr,uid,ids).id
		# weekly_plans = self.pool.get('weekly.plans').search(cr, uid, [('cust','=',cust_id)])
		# models_data = self.pool.get('ir.model.data')
		# weekly_plans_tree = models_data._get_id(cr, uid, 'athleat', 'weekly_plan_tree_view')
		# return {
		# 	'name': 'Weekly Plans',
		# 	'view_type': 'form',
		# 	"view_mode": 'tree,form',
		# 	'res_model': 'weekly.plans',
		# 	'type': 'ir.actions.act_window',
		# 	'search_view_id': weekly_plans_tree,
		# 	'domain': "[('id', 'in',%s)]" % (weekly_plans),
		# }

Customer()




class Pause_Activate(osv.osv):

	_name = 'plan.action.requests'
	
	_rec_name = 'customer_id'

	def _customer_id(self, cr, uid, context=None):
		user = self.pool.get('res.users').browse(cr, uid, uid)
		return user.id


	_columns = {
	# 'customer_id': fields.many2one('res.users','Customer', required=True),
	'customer_id': fields.many2one('res.partner','Customer', domain=[('customer','=', True)], required=True),
	'plan_id': fields.many2one('meal.plans','Meal Plan', domain=[('state','=', 'active')]),
	'submitted_at': fields.datetime('Submitted At'),
	'pause_date': fields.date('Pause Date/Activate'),
	'request_type': fields.selection([('pause','Pause'),
										('activate','Activate')],'Request Type', required=True),
	'status': fields.selection([
								('draft','Draft'),
								('submitted','Submitted'),
								('approved','Approved')],'Status', required=True),
	'next_count':fields.integer('Count'),
	# 'submitted_date': fields.date('Submitted Date'),
	}

	_defaults = {
	# 'customer_id':_customer_id,
	'status': 'draft',
	'submitted_at': datetime.now(),
	# 'submitted_date':datetime.today()
	}

	def create(self, cr, uid, vals, context=None):
		weeks = self.pool.get('meal.plans').browse(cr,uid,vals['plan_id']).no_of_weeks
		time = self.pool.get('meal.plans').browse(cr,uid,vals['plan_id']).expiry_date
		date = datetime.strptime(time, '%Y-%m-%d')
		total = len(self.search(cr,uid,[('plan_id','=',vals['plan_id'])]))
		
		if weeks == 4 and datetime.today()<date and total>=2 :raise osv.except_osv(_('Sorry!'),_('You have exceeded your limit.'))
		elif weeks == 8 and datetime.today()<date and total>=4 :raise osv.except_osv(_('Sorry!'),_('You have exceeded your limit.'))
		elif weeks == 12 and datetime.today()<date and total>=6 :raise osv.except_osv(_('Sorry!'),_('You have exceeded your limit.'))
		else: return super(Pause_Activate, self).create(cr, uid, vals, context=context)


	def unlink(self, cr, uid, ids, context=None):
		for obj in self.browse(cr, uid, ids):
			if obj.status in ('submitted','approved'):
				raise osv.except_osv(_('Invalid Action!'),_('You cannot delete a record which is Submitted/Approved.'))
		return super(Pause_Activate, self).unlink(cr, uid, ids, context=context)

	def onchange_customer_id(self, cr, uid, ids, customer_id, context=None):
		plan = self.pool.get('meal.plans').search(cr, uid, [('customer.related_user', '=',customer_id),('state','=','active')], context=context)
		
		if plan:
			plan = plan[0]
		return {'value':{'plan_id':plan}}

	
	def submit(self, cr, uid, ids, context=None):
		self.write(cr, uid, ids, {'status': 'submitted'})

		admin_mail_list =[]
		manager_mail_list =[]
		
		# searching for users with access level as admin
		admin_list = self.pool.get('res.users').search(cr, uid, [('groups_id.name','=','Superadmin'), ('groups_id.category_id.name','=','Meal Plans Access Levels')])
		for i in admin_list:
			admin_mail_list += [str(self.pool.get('res.users').browse(cr,uid,i).email)]
		
		# searching for users with access level as manager
		manager_list = self.pool.get('res.users').search(cr, uid, [('groups_id.name','=','Manager'), ('groups_id.category_id.name','=','Meal Plans Access Levels')])#group id based on installation

		for x in manager_list:
			manager_mail_list += [str(self.pool.get('res.users').browse(cr,uid,x).email)]
		complete_mails = admin_mail_list + manager_mail_list#adding superadmin mails and manager mail ids
	
		if complete_mails:
			for y in complete_mails:
				if y != False:
					template_obj = self.pool.get('email.template')
					group_model_id = self.pool.get('ir.model').search(cr, uid, [('model', '=', 'plan.action.requests')])[0]
					
					body_html = ''' <div><p style="background:white"><span style="font-size:10.0pt;font-family:helvetica,sans-serif;color:black">
							Dear Admin/Manager,</span></p></span></div>
							New request for Pause/Activate Requests has been generated.
						   '''
					template_data = {
						'model_id': group_model_id,
						'name': 'Request Notification',
						'subject' : 'Pause/Activate Request',
						'body_html': body_html,
						'email_from' : '${object.customer_id.email}',
						'email_to' : y,
					}

					template_id = template_obj.create(cr, uid, template_data, context=context)
					template_obj.send_mail(cr, uid, template_id, ids[0], force_send=True, context=context)
				else:
					pass
		return True
		
	def approve(self, cr, uid, ids, context=None):
		self.write(cr, uid, ids, {'status': 'approved'})

		cust = self.browse(cr,uid,ids).customer_id.id
		cust_mail = self.browse(cr,uid,ids).customer_id.email
		req_type = self.browse(cr,uid,ids).request_type

		# changing customer's plan status

		meal = self.pool.get('meal.plans').search(cr, uid, [('customer.related_user', '=',cust)], context=context)

		if req_type == 'pause':
			self.pool.get('meal.plans').write(cr, uid, meal, {'state': 'paused'})
		else:
			self.pool.get('meal.plans').write(cr, uid, meal, {'state': 'active'})
		
		# notifying customer through mail
		template_obj = self.pool.get('email.template')
		group_model_id = self.pool.get('ir.model').search(cr, uid, [('model', '=', 'plan.action.requests')])[0]
		
		body_html = ''' <div><p style="background:white"><span style="font-size:10.0pt;font-family:helvetica,sans-serif;color:black">Dear ${object.customer_id.name},</span></p></span></div>
					<p> Your Meal plan details are as follows:</p>
					<p><b>No. of Weeks : </b> ${object.plan_id.no_of_weeks}</p>
					<p><b>Days per week : </b> ${object.plan_id.days_per_week}</p>
					<p><b>No of meals per day: </b> ${object.plan_id.meals_per_day}</p>
					<p><b>Meal Plan Type: </b> ${object.plan_id.meal_plan_type}</p>
					<p><b>Starting when: </b> ${object.plan_id.current_date}</p>
			   
					<p><b>Status: </b> ${object.plan_id.state}</p>
			   '''
		template_data = {
			'model_id': group_model_id,
			'name': 'Customer Notification',
			'subject' : 'Meal Plan Details',
			'body_html': body_html,
			'email_from' : 'hello@athleat.ae',
			'email_to' : '${object.customer_id.email}',
		}

		template_id = template_obj.create(cr, uid, template_data, context=context)
		template_obj.send_mail(cr, uid, template_id, ids[0], force_send=True, context=context)

		return True




class like_dislike_requests(osv.osv):

	_name = 'likes.dislikes.requests'

	_rec_name = 'customer_id'

	_columns = {
	'customer_id': fields.many2one('res.partner','Customer', domain=[('customer','=', True)], required=True),
	'plan_id': fields.many2one('meal.plans','Meal Plan', domain=[('state','=', 'active')]),
	'items':fields.one2many('liked.disliked.meals','req_id','Meal Items'),
	'submitted_at': fields.datetime('Submitted At'),
	'request_type': fields.selection([('like','Like'),
		('dislike','Dislike')],'Request Type', required=True),
	'status': fields.selection([
								('draft','Draft'),
								('submitted','Submitted'),
								('approved','Approved')],'Status', required=True),

	}

	_defaults = {
	'status': 'draft',
	'submitted_at': datetime.now(),
	}

	# def create(self, cr, uid, vals, context=None):
	# 	total = len(self.search(cr,uid,[('customer_id','=',vals['customer_id'])]))
		
	# 	# rec = self.pool.get('meal.plans').search(cr,uid,[('id','=',vals['plan_id']),('state','=','active'),('expiry_date','>',vals['submitted_at'])])
	# 	# if rec:
	# 	# 	if rec[0] and total >= 4: raise osv.except_osv(_('Sorry!'),_('You have exceeded your limit.'))
		
	# 	return super(like_dislike_requests, self).create(cr, uid, vals, context=context)

	def unlink(self, cr, uid, ids, context=None):
		for obj in self.browse(cr, uid, ids):
			if obj.status in ('submitted','approved'):
				raise osv.except_osv(_('Invalid Action!'),_('You cannot delete a record which is Submitted/Approved.'))

		return super(like_dislike_requests, self).unlink(cr, uid, ids, context=context)

	def onchange_customer_id(self, cr, uid, ids, customer_id, context=None):
		plan = self.pool.get('meal.plans').search(cr, uid, [('customer', '=',customer_id)], context=context)
		if plan:
			plan = plan[0]
		return {'value':{'plan_id':plan}}


	def submit(self, cr, uid, ids, context=None):
		self.write(cr, uid, ids, {'status': 'submitted'})

		admin_mail_list =[]
		manager_mail_list =[]

		## searching for users with access level as admin
		admin_list = self.pool.get('res.users').search(cr, uid, [('groups_id', '=', 59)])
		for i in admin_list:
			admin_mail_list += [str(self.pool.get('res.users').browse(cr,uid,i).email)]

		# searching for users with access level as manager
		manager_list = self.pool.get('res.users').search(cr, uid, [('groups_id', '=', 60)])
		for x in manager_list:
			manager_mail_list += [str(self.pool.get('res.users').browse(cr,uid,x).email)]
		 
		complete_mails = admin_mail_list + manager_mail_list#adding superadmin mails and manager mail ids
	
		for y in complete_mails:
			if y != False:
				template_obj = self.pool.get('email.template')
				group_model_id = self.pool.get('ir.model').search(cr, uid, [('model', '=', 'plan.action.requests')])[0]
				
				body_html =''' <div><p style="background:white"><span style="font-size:10.0pt;font-family:helvetica,sans-serif;color:black">
				Dear Admin/Manager,</span></p></span></div>
				New request for Likes/Allergies/Dislikes Requests has been generated.
			   '''
				template_data = {
					'model_id': group_model_id,
					'name': 'Request Notification',
					'subject' : 'Likes/Allergies/Dislikes Requests',
					'body_html': body_html,
					'email_from' : '${object.customer_id.email}',
					'email_to' : y,
				}

				template_id = template_obj.create(cr, uid, template_data, context=context)
				template_obj.send_mail(cr, uid, template_id, ids[0], force_send=True, context=context)
			else:
				pass
		return True



	def approve(self, cr, uid, ids, context=None):
		# liked_items = []
		# disliked_items = []
		# non_veg_items=[]
		# meal_items_final=[]
		return self.write(cr, uid, ids, {'status': 'approved'})

		# req_type = self.browse(cr,uid,ids).request_type
		# cust_id =self.browse(cr,uid,ids).customer_id.id
		# cust = self.pool.get('res.partner').search(cr, uid, [('id', '=',cust_id)], context=context)[0]

		# veg_nonveg = self.pool.get('res.partner').browse(cr, uid, cust_id).meal_type
		# veg_meals_list = self.pool.get('recipies.meal').search(cr, uid, [('meal_preference', '=', 'veg')], context=context)
		# chick_meals_list = self.pool.get('recipies.meal').search(cr, uid, [('meal_preference', '=', 'chick')], context=context)
		# fish_meals_list = self.pool.get('recipies.meal').search(cr, uid, [('meal_preference', '=', 'fish')], context=context)
		# lamb_meals_list = self.pool.get('recipies.meal').search(cr, uid, [('meal_preference', '=', 'lamb')], context=context)
		# beef_meals_list = self.pool.get('recipies.meal').search(cr, uid, [('meal_preference', '=', 'beef')], context=context)
		# if chick_meals_list:
		# 	non_veg_items +=  chick_meals_list
		# elif fish_meals_list:
		# 	non_veg_items +=  fish_meals_list
		# elif lamb_meals_list:
		# 	non_veg_items +=  lamb_meals_list
		# elif veg_meals_list:
		# 	non_veg_items +=  veg_meals_list
		# elif beef_meals_list:
		# 	non_veg_items +=  beef_meals_list
		# else:
		# 	pass
		# if veg_nonveg == 'veg':
		# 	recipies = veg_meals_list
		# else:
		# 	recipies = non_veg_items

		# cust_liked = self.pool.get('res.partner').browse(cr, uid, cust_id).liked_meals_ids.ids
		# if cust_liked:
		# 	# appending old items
		# 	liked_items+=cust_liked

		# cust_disliked = self.pool.get('res.partner').browse(cr, uid, cust_id).disliked_meals_ids.ids
		# if cust_disliked:
		# 	# appending old items
		# 	disliked_items+=cust_disliked

		# items_list = self.browse(cr,uid,ids).items.ids
		# if req_type == 'like':

		# 	for i in items_list:
		# 		item=self.pool.get('liked.disliked.meals').browse(cr,uid,i).name.id
		# 		liked_items += [item]
		# 		if item in cust_disliked:
		# 			cust_disliked.remove(item)
		# 	# self.pool.get('res.partner').write(cr, uid, cust, {'disliked_meals_ids': [(6, 0, cust_disliked)]})
		# 	# self.pool.get('res.partner').write(cr, uid, cust, {'liked_meals_ids': [(6, 0, liked_items)]})

		# else:
		# 	for i in items_list:
		# 		item=self.pool.get('liked.disliked.meals').browse(cr,uid,i).name.id
		# 		disliked_items += [item]
		# 		if item in cust_liked:
		# 			cust_liked.remove(item)
		# 	self.pool.get('res.partner').write(cr, uid, cust, {'liked_meals_ids': [(6, 0, cust_liked)]})
		# 	self.pool.get('res.partner').write(cr, uid, cust, {'disliked_meals_ids': [(6, 0, disliked_items)]})

		
		
		# return True
		



# class custom_users(osv.osv):
# 	# _name = 'res.users'
	# _inherit = 'res.users'

# 	def create(self,cr,uid,vals,context=None):
# 		context.update({'no_reset_password':True})
# 		return super(custom_users,self).create(cr,uid,vals,context=context)

	# def create(self,cr,uid,vals,context=None):
	# 	values ={
	# 	# 'notify_email':'always',
	# 	'name':vals.get('name'),
	# 	'email':vals.get('email'),
	# 	'create_date':datetime.now(),
	# 	'write_date':datetime.now(),
	# 	'create_uid':1,
	# 	'write_uid':1,
	# 	'is_company':False,
	# 	'customer':False,
	# 	'active':True,
	# 	'display_groups_suggestions': True, 
	# 	'alias_contact': 'everyone', 
	# 	'company_ids': [[6, False, [1]]],
	# 	'lang': 'en_US', 

	# 	}
	# 	part=self.pool.get('res.partner').create(cr,uid,values,context=context)

	# 	vals={	
	# 			'name':vals.get('name'),
	# 			'login': vals.get('login'),
	# 			'create_uid': 1, 
	# 			'share': True, 
	# 			'write_uid': 1, 
	# 			'display_groups_suggestions': True,
	# 			'active': True, 
	# 			'login': False, 
	# 			'partner_id': part,
	# 		}
		
	# 	return super(custom_users,self).create(cr,uid,vals,context=context)


	# @api.model
	# def create(self, values):

	#     return super(custom_users, self).create(values)

	

class Customer(osv.osv):
	_inherit = 'res.users'
	
	_columns = {
	'existing_customer' :fields.boolean('Existing Customer?'),
	'dummy':fields.char('Dummy Password'),
	}
	
	def create(self,cr,uid,vals,context=None):
		context.update({'no_reset_password':True})
		return super(Customer,self).create(cr,uid,vals,context=context)

	def custom_action_reset_password(self, cr, uid, ids, context=None):
		password = ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$*') for i in range(6))
		self.browse(cr,uid,ids).write({'password':password,'dummy':password})

		res_partner = self.pool.get('res.partner')
		partner_ids = [user.partner_id.id for user in self.browse(cr, uid, ids, context)]
		res_partner.signup_prepare(cr, uid, partner_ids, signup_type="reset", expiration=now(days=+1), context=context)

		context = dict(context or {})

		template = False
		if context.get('create_user'):
			try:
				template = self.pool.get('ir.model.data').get_object(cr, uid, 'athleat', 'custom_set_password_email')
			except ValueError:
				pass
		# if not bool(template):
		# 	template = self.pool.get('ir.model.data').get_object(cr, uid, 'athleat', 'custom_reset_password_email')
		assert template._name == 'email.template'

		for user in self.browse(cr, uid, ids, context):
			if not user.email:
				raise osv.except_osv(_("Cannot send email: user has no email address."), user.name)
			context['lang'] = user.lang 
			self.pool.get('email.template').send_mail(cr, uid, template.id, user.id, force_send=True, raise_exception=True, context=context)



class Password_length_check(osv.TransientModel):
	_inherit = 'change.password.wizard'

	# @api.onchange('new_passwd')
	# def _onchange_password(self):
	# 	if self.new_passwd:
	# 		if len(self.new_passwd)<6:
	# 			raise Warning(_('Warning!!'), _('Password must be greater than 6 characters!!'))

	def change_password_button(self, cr, uid, ids, context=None):
		wizard = self.browse(cr, uid, ids, context=context)[0]
		
		need_reload = any(uid == user.user_id.id for user in wizard.user_ids)

		line_ids = [user.id for user in wizard.user_ids]
		for i in line_ids:
			password = self.pool.get('change.password.user').browse(cr,uid,i).new_passwd
			if len(password) < 6 :
				raise Warning(_('Warning!!'), _('Password must be greater than 6 characters!!'))
				
		self.pool.get('change.password.user').change_password_button(cr, uid, line_ids, context=context)

		if need_reload:
			return {
				'type': 'ir.actions.client',
				'tag': 'reload'
			}

		return {'type': 'ir.actions.act_window_close'}


	