from openerp.osv import osv,fields
from datetime import datetime, timedelta
from openerp import api

class Plan_analysis(osv.osv):
	
	_name = "plan.analysis"	

	_columns = {

	'customer_id':fields.many2one('res.partner','Customer'),
	'served_meals_count':fields.integer('Served'),
	'pending_meals_count':fields.integer('Pending Meals Details'),
	'days_left':fields.integer('Days left in Meal Plan'),
	'carb_type':fields.selection([('high carb','High Carb'),
								('low carb','Low Carb'),
								('customize','Customized')],'Carb Type'),
	'total_meals_id':fields.one2many('meal.meal','plan_analysis_id','Weekly Meals'),

	}
	
	@api.onchange('customer_id')
	def _onchange_customer_id(self):
		try:
			if self.customer_id.carb_type:
				self.carb_type = self.customer_id.carb_type
		except Exception as e:
			pass

	def fetching_all_plan_details(self,cr,uid,context=None):
		count_served = 0
		pending_days = 0
		values = {}
		confirm_scheduled_plans = self.pool.get('weekly.plans').search(cr,uid,[('state','=','confirm')])
	
		for i in confirm_scheduled_plans:
			
			rec= self.pool.get('weekly.plans').browse(cr,uid,i)
			cust_id = rec.cust.id
			carb_type = rec.cust.carb_type
			plan_type = rec.meal_plan_type
			meals_list = rec.meals_id1.ids+rec.meals_id2.ids+rec.meals_id3.ids+rec.meals_id4.ids+rec.meals_id5.ids+rec.meals_id6.ids+rec.meals_id7.ids+rec.meals_id8.ids+rec.meals_id9.ids+rec.meals_id10.ids+rec.meals_id11.ids+rec.meals_id12.ids


			for x in meals_list:
				meal_meal = self.pool.get('meal.meal').browse(cr,uid,x)

				if datetime.today() > datetime.strptime(meal_meal.date, '%Y-%m-%d'):
					count_served += 1
				else:
					pending_days += 1
					idd=self.pool.get('meal.meal').search(cr,uid,[('id','=',x)])
					values.update({'meals_per_day':rec.meals_per_day,})
					self.pool.get('meal.meal').write(cr,uid,idd, values)
			
			vals={
				'customer_id':cust_id,
				'served_meals_count':count_served,
				'days_left':pending_days,
				'carb_type':carb_type,
				'total_meals_id':[(6,0,meals_list)]
				}
			

			exists = self.search(cr,uid,[('customer_id','=',cust_id)])
			if exists:
				self.write(cr,uid,exists, vals)
			else:
				self.create(cr,uid,vals,context=context)

		return True



	
Plan_analysis()
