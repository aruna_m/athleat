from openerp.osv import osv, fields
import xlrd
import os
from xlrd import open_workbook
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
import datetime
from datetime import datetime,timedelta
import socket
import xmlrpclib
from openerp import api,_

username = 'admin' #the user
pwd = 'admin'      #the password of the user
dbname = 'athleat'    #the database

URL = 'http://localhost:9069/xmlrpc/'
sock_common = xmlrpclib.ServerProxy(URL + 'common')
sock = xmlrpclib.ServerProxy(URL + 'object')

class Meal_plans_upload(osv.osv_memory):
	_name = "plans.upload"

	_columns={
		'upload_document':fields.binary('Upload Document'),
		'filename':fields.char('Filename'),
	}

	def upload_meal_plans_data(self,cr,uid,ids,context=None):
		
		input_data = self.read(cr, uid, ids[0], context=context)

		filename = input_data['filename']
		if not ('.xls' in filename or '.xlsx' in filename or '.txt' in filename):
			raise Warning(_('Please choose .xls/.xlsx/.txt files only!!'))

		possible_strings = ['Name','Number','Email','Location','Meal Plan','Remarks section for internal notes','Allergies/Dislikes','Days Left','No.of Weeks']

		xl_workbook = open_workbook(file_contents=base64.decodestring(input_data['upload_document']))

		sheet_name = xl_workbook.sheet_names()
			
		for each in sheet_name:
			actual_row, name_col, number_col, email_col, location_col, meal_plan_col, remarks_col, allergies_col, daysleft_col,weeksno_col = False, False, False, False, False, False, False, False, False, False
			xl_sheet = xl_workbook.sheet_by_name(each)

			for row in range(0, xl_sheet.nrows):

				for col in range(0, xl_sheet.ncols):

					if xl_sheet.cell(row, col).value in possible_strings: actual_row = row + 1
					
					if xl_sheet.cell(row, col).value in ['Name']:
						name_col = col
					elif xl_sheet.cell(row, col).value in ['Number']:
						number_col = col
					elif xl_sheet.cell(row, col).value in ['Email']:
						email_col = col
					elif xl_sheet.cell(row, col).value in ['Location']:
						location_col = col
					elif xl_sheet.cell(row, col).value in ['Meal Plan']:
						meal_plan_col = col
					elif xl_sheet.cell(row, col).value in ['Remarks section for internal notes']:
						remarks_col = col
					elif xl_sheet.cell(row, col).value in ['Allergies/Dislikes']:
						allergies_col = col
					elif xl_sheet.cell(row, col).value in ['Days Left']:
						daysleft_col = col
					elif xl_sheet.cell(row, col).value in ['No.of Weeks']:
						weeksno_col = col
					else:
						pass

			for row in range(actual_row, xl_sheet.nrows):
				vals, values = {}, {}

				name, number, email, location, meal_plan, remarks, allergies, days_left, weeks = False, False, False, False, False, False, False, False, False

				vals.update({'days_per_week':5})
				if name_col : name = xl_sheet.cell(row, int(name_col)).value
				if number_col : number = xl_sheet.cell(row, int(number_col)).value
				if email_col : email = xl_sheet.cell(row, int(email_col)).value
				if location_col : location = xl_sheet.cell(row, int(location_col)).value
				if meal_plan_col : meal_plan = xl_sheet.cell(row, int(meal_plan_col)).value
				if remarks_col : remarks = xl_sheet.cell(row, int(remarks_col)).value
				if allergies_col : allergies = xl_sheet.cell(row, int(allergies_col)).value
				if daysleft_col : days_left = xl_sheet.cell(row, int(daysleft_col)).value
				if weeksno_col: weeks =  xl_sheet.cell(row, int(weeksno_col)).value
				
				if name:
					if name : values.update({'name':name})
					if number : values.update({'contact_no':number})
					if email : values.update({'email':email})
					if location : values.update({'street':location})

					if weeks: vals.update({'no_of_weeks':int(weeks)})
					if days_left : 
						expiry_date =  (datetime.now() + timedelta(days=int(days_left)+1)).date()
						days = int(weeks)*5
						current_date = (datetime.now() - timedelta(days=int(days)-int(days_left))).date()
						vals.update({'expiry_date':str(expiry_date),'current_date':str(current_date)})

					if meal_plan : 
						values.update({'carb_type':meal_plan})
						vals.update({'meal_plan_type':meal_plan})

					if remarks : vals.update({'spl_req':remarks})
					if allergies:
						total = []
						dislikes = allergies.split(',')
						for each in dislikes:
							exists = self.pool.get('likes.dislikes').search(cr,uid,[('name','=',each),('type','=','dislikes')])
							if exists: total += [exists[0]]
							else: total += [sock.execute(dbname, uid, pwd, 'likes.dislikes','create',{'name':each,'type':'dislikes'})]
						
						values.update({'disliked_meals2_ids':[(6,0,total)]})


					if values:
						exists = self.pool.get('res.partner').search(cr,uid,[('name','ilike',name),('contact_no','ilike',number),('email','ilike',email)])
						if not exists:	customer = sock.execute(dbname, uid, pwd, 'res.partner','create', values)
						else: customer = exists[0]

					vals.update({'customer':customer,'breakfast':1,'mid':1,'evening':1,'night':1})
					if customer and vals:
						plan = self.pool.get('meal.plans').search(cr,uid,[('customer','=',customer),('meal_plan_type','=',meal_plan)])
						if plan:
							idd = plan[0]
							self.pool.get('meal.plans').browse(cr,uid,plan[0]).write(vals)
						else:
							idd = sock.execute(dbname, uid, pwd, 'meal.plans', 'create', vals)

						

						