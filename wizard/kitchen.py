from openerp.osv import osv,fields
import datetime
from datetime import datetime
from collections import Counter
import time
import pytz
from openerp import tools
from openerp.report import report_sxw

import urllib
import os
import xlwt
# import xlrd
import base64

class Display_meals_wizard(osv.osv_memory):

	_name="kitchen.meals"

	_columns={

	'date':fields.date('Date', required="1"),
	'meal_type':fields.selection([('morning','Morning'),
								('mid','MID'),
								('evening','Evening')],'Meal Type', required="1"),

	}

	def print_kitchen_report(self,cr,uid,ids,context=None):
		vals=self.read(cr, uid, ids[0], context=context)
		datas = {
		'form': vals,
		}
		return {'type': 'ir.actions.report.xml', 'report_name': 'athleat.kitchen_report_meals', 'datas': datas,'context':context}


		
	
class Kitchen_wizard_ext(report_sxw.rml_parse):

	def __init__(self, cr, uid, name, context):
		super(Kitchen_wizard_ext, self).__init__(cr, uid, name, context=context)

		self.localcontext.update({
			'date': self._get_date,
			'recipies': self._get_recipies,
		})

	def _get_date(self, form):
		date = self._get_utc_time_range(form)


	def _get_recipies(self, form):
		data = []
		date = form['date']
		plans = []
		high, low, customize = [],[],[]
		
		delivery_time = form['meal_type']
		if delivery_time == 'morning' : plans += self.pool.get('meal.plans').search(self.cr,self.uid,[('state','=','active'),('delivery_timings','=','11am-1.30pm')])
		if delivery_time == 'mid' : plans += self.pool.get('meal.plans').search(self.cr,self.uid,[('state','=','active'),('delivery_timings','=','4pm-6pm')])
		if delivery_time == 'evening' : plans += self.pool.get('meal.plans').search(self.cr,self.uid,[('state','=','active'),('delivery_timings','=','7:45pm-9:45pm')])
		print "plansssssssssssss",plans
		if plans:
			for each in plans:
				exclusions=''
				addons=''
				rec = self.pool.get('meal.plans').browse(self.cr,self.uid,each)
				for add in rec.addons.ids:
					addons += str(self.pool.get('addons.conf').browse(self.cr,self.uid,add).name)+','

				for exl in rec.customer.disliked_meals2_ids.ids:
					exclusions += str(self.pool.get('likes.dislikes').browse(self.cr,self.uid,exl).name)+','
				customer_id = rec.customer.id
				meals = self.pool.get('meal.meal').search(self.cr,self.uid,[('cust','=',customer_id),('date','=',date)])
				if meals:
					for meal in meals:
						rex = self.pool.get('meal.meal').browse(self.cr,self.uid,meal)
						if rex.carb_type == 'low carb':
							low+=[{'name':rec.customer.name,'addons':addons,'exclusion':exclusions,'m1':[rex.item1.name],'m2':[rex.item2.name],'m3':[rex.item3.name],'m4':[rex.item4.name]}]
						
						if rex.carb_type == 'high carb':
							high+=[{'name':rec.customer.name,'addons':addons,'exclusion':exclusions,'m1':[rex.item1.name],'m2':[rex.item2.name],'m3':[rex.item3.name],'m4':[rex.item4.name]}]
						
						elif rex.carb_type == 'customize':
							customize +=[{'name':rec.customer.name,
										'exclusion':exclusions,
										'addons':addons,
										'm1':[rex.meal11,rex.meal12,rex.meal13,rex.sauce1],
										'm2':[rex.meal21,rex.meal22,rex.meal23,rex.sauce2],
										'm3':[rex.meal31,rex.meal32,rex.meal33,rex.sauce3],
										'm4':[rex.meal41,rex.meal42,rex.meal43,rex.sauce4],}]
						else:pass
						
		result = {'high': high,'low':low,'customize':customize}
		
		data.append(result)

		if data:return data
		else:return {}


class kitchen_report_view_meal_details(osv.AbstractModel):
	_name = 'report.athleat.kitchen_report_meals'
	_inherit = 'report.abstract_report'
	_template = 'athleat.kitchen_report_meals'
	_wrapped_report_class = Kitchen_wizard_ext
