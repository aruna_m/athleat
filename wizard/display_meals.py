from openerp.osv import osv,fields
import datetime
from datetime import datetime
from collections import Counter
import time
import pytz
from openerp import tools
from openerp.report import report_sxw

import urllib
import os
import xlwt
# import xlrd
import base64

class Display_meals_wizard(osv.osv_memory):

	_name="display.meals1"

	_columns={

	'start_date':fields.date('Start Date', required="1"),
	'end_date': fields.date('End Date', required="1"),
	'report_type':fields.selection([('daily_report','Meal Count (From/To Dates)'),
									('customer_report','Customer Meal Report')],'Report Type', required="1"),

	}

	def print_meals_report(self,cr,uid,ids,context=None):
		vals=self.read(cr, uid, ids[0], context=context)
		datas = {
		'form': vals,
		}
		if vals['report_type']=='daily_report':
			return {'type': 'ir.actions.report.xml', 'report_name': 'athleat.display_meals_list', 'datas': datas,'context':context}
		elif vals['report_type']=='customer_report':
			return {'type': 'ir.actions.report.xml', 'report_name': 'athleat.display_customer_meals_list', 'datas': datas,'context':context}


	# def print_xls_meals_report(self,cr,uid,ids,context=None):
	# 	vals=self.read(cr, uid, ids[0], context=context)
	
	# 	wb = xlwt.Workbook()
	# 	ws = wb.add_sheet('Meal Details')

	# 	names=['Customer Name','Meal Type','No. Of Meals','Special Request','Addons','Meals']
	# 	for i in names:
	# 		ws.write(0,names.index(i),i)

	# 	regular = self.pool.get('meal.meal').search(cr,uid,[('carb_type','=','high carb'),('status','=','scheduled'),('date','>=',vals['start_date']),('date','<=',vals['end_date'])])
	# 	lowcarb = self.pool.get('meal.meal').search(cr,uid,[('carb_type','=','low carb'),('status','=','scheduled'),('date','>=',vals['start_date']),('date','<=',vals['end_date'])])
	# 	customize = self.pool.get('meal.meal').search(cr,uid,[('carb_type','=','customize'),('status','=','scheduled'),('date','>=',vals['start_date']),('date','<=',vals['end_date'])])
		
	# 	for x in [regular,lowcarb,customize]:
	# 		if x==regular: row_no, carb_type, types = 2,'Regular', 'high carb'
	# 		elif x==lowcarb: row_no, carb_type, types = 2+len(regular),'Low Carb', 'low carb'
	# 		elif x==customize: row_no, carb_type, types = 2+len(lowcarb)+len(regular),'Customized','customize'
	# 		else:pass

	# 		for i in x:
	# 			meal = self.pool.get('meal.meal').browse(cr,uid,i)
	# 			rec=self.pool.get('meal.plans').search(cr,uid,[('customer','=',meal.cust.id),('meal_plan_type','=',types)])
	# 			if rec:
	# 				ws.write(row_no+x.index(i), 0, meal.cust.name)
	# 				ws.write(row_no+x.index(i), 1, carb_type)
	# 				ws.write(row_no+x.index(i), 2, self.pool.get('meal.plans').browse(cr,uid,rec[0]).meals_per_day)
	# 				ws.write(row_no+x.index(i), 3, meal.spl_req)
	# 				ws.write(row_no+x.index(i), 4, 'addons')
	# 				ws.write(row_no+x.index(i), 5, meal.customized_scheduled_meals)

	# 	wb.save('example.xls')
	# 	print "9999",self.pool.get('ir.attachment').create(cr, uid,
	# 				{
	# 					'name': 'file_name',
	# 					'datas': base64.encode(ws.read()),
	# 					'datas_fname': 'file_name',
	# 					# 'res_model': self._name,
	# 					# 'res_id': record.id,
	# 					'type': 'binary'
	# 				}, context=context)

		

		
	
class details_wizard2(report_sxw.rml_parse):

	def __init__(self, cr, uid, name, context):
		super(details_wizard2, self).__init__(cr, uid, name, context=context)
		
		self.localcontext.update({
			'time': time,
			'date_details':self._get_dates,
			'recipies':self._get_recipies,
		})

	def _get_dates(self, form):

		date_start, date_end = self._get_utc_time_range(form)

		
	def _get_recipies(self,form):
		data = []
		x={}
		y={}
		z={}
		x_cust={}
		y_cust={}
		z_cust={}
		
		total_regular_items=[]
		total_lowcarb_items=[]
		total_customzied_items=[]
		

		regular = self.pool.get('meal.meal').search(self.cr,self.uid,[('carb_type','=','high carb'),('date','>=',form['start_date']),('date','<=',form['end_date'])])
		for i in regular:
			meals=[]
			rec=self.pool.get('meal.meal').browse(self.cr,self.uid,i)

			item1, item2, item3, item4 = rec.item1.id, rec.item2.id, rec.item3.id, rec.item4.id
			
			for each in [item1, item2, item3, item4]:
				if each == item1:	meal_no = 'BREAKFAST'
				elif each == item2:	 meal_no = 'MID'
				elif each == item3:	 meal_no = 'EVENING'
				elif each == item4: meal_no = 'NIGHT'
				else:pass
				meals += [str(self.pool.get('recipies.meal').browse(self.cr,self.uid,each).name)+'('+str(self.pool.get('recipies.meal').browse(self.cr,self.uid,each).quantity)+')'+ '(' +meal_no+')']
			
			total_regular_items += [item1] + [item2] + [item3] + [item4]
			x_cust.update({rec.cust.name:meals})

		for ls in set(total_regular_items):
			x.update({str(self.pool.get('recipies.meal').browse(self.cr,self.uid,ls).name):total_regular_items.count(ls)})


		lowcarb = self.pool.get('meal.meal').search(self.cr,self.uid,[('carb_type','=','low carb'),('date','>=',form['start_date']),('date','<=',form['end_date'])])
		for i in lowcarb:
			meals=[]
			rec=self.pool.get('meal.meal').browse(self.cr,self.uid,i)

			item1, item2, item3, item4 = rec.item1.id, rec.item2.id, rec.item3.id, rec.item4.id
			
			for each in [item1, item2, item3, item4]:
				if each == item1:	meal_no = 'BREAKFAST'
				elif each == item2:	 meal_no = 'MID'
				elif each == item3:	 meal_no = 'EVENING'
				elif each == item4: meal_no = 'NIGHT'
				else:pass
				meals += [str(self.pool.get('recipies.meal').browse(self.cr,self.uid,each).name)+'('+str(self.pool.get('recipies.meal').browse(self.cr,self.uid,each).quantity)+')'+ '(' +meal_no+')']
			
			total_lowcarb_items += [item1] + [item2] + [item3] + [item4]
			y_cust.update({rec.cust.name:meals})

		for ls in set(total_lowcarb_items):
			y.update({str(self.pool.get('recipies.meal').browse(self.cr,self.uid,ls).name):total_lowcarb_items.count(ls)})



		customize = self.pool.get('meal.meal').search(self.cr,self.uid,[('carb_type','=','customize'),('date','>=',form['start_date']),('date','<=',form['end_date'])])
		for i in customize:
			cust_name=self.pool.get('meal.meal').browse(self.cr,self.uid,i).cust.name

			abj = self.pool.get('meal.meal').browse(self.cr,self.uid,i)
			total_customzied_items += [abj.cusitem1] + [abj.cusitem2] + [abj.cusitem3] + [abj.cusitem4]
			z_cust.update({cust_name:[abj.cusitem1] + [abj.cusitem2] + [abj.cusitem3] + [abj.cusitem4]})

		for ls in set(total_customzied_items):
			z.update({str(ls):1})


		result = {
			'regular_cust':x_cust,
			'regular':x,

			'lowcarb':y,
			'lowcarb_cust':y_cust,
			
			'customize':z,
			'customize_cust':z_cust,
		}
		data.append(result)

		if data:
			return data
		else:
			return {}

class report_view_meal_details(osv.AbstractModel):
	_name = 'report.athleat.display_meals_list'
	_inherit = 'report.abstract_report'
	_template = 'athleat.display_meals_list'
	_wrapped_report_class = details_wizard2



class report_view_cust_meal_details(osv.AbstractModel):
	_name = 'report.athleat.display_customer_meals_list'
	_inherit = 'report.abstract_report'
	_template = 'athleat.display_customer_meals_list'
	_wrapped_report_class = details_wizard2


