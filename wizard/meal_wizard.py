from openerp.osv import osv,fields
from openerp import api,_
import random
from datetime import datetime, timedelta, date
from openerp.exceptions import except_orm, Warning, RedirectWarning

def date_by_adding_business_days(from_date, add_days):
	business_days_to_add = add_days
	current_date = from_date
	while business_days_to_add > 0:
		current_date += timedelta(days=1)
		weekday = current_date.weekday()
		if from_date.weekday() == 6:
			if weekday == 5 or weekday == 4: # sunday = 6
				continue
		
		business_days_to_add -= 1
	return current_date

class Wizard_Meal(osv.TransientModel):
	_name="wizard.meal"

	@api.model
	def _default_note(self):
		d =  datetime.now()
		t = timedelta((7 + 5 - d.weekday()) % 7)
		sat_date = (d + t).strftime('%d-%m-%Y')
		date =  datetime.strptime(sat_date , '%d-%m-%Y')
		return date


	_columns={

	'current_date':fields.date('Restart Date',default=_default_note),
	'no_of_weeks': fields.selection([(4,'4'),
											(8,'8'),
											(12,'12')],'How many Weeks ?'),
	'days_per_week': fields.selection([(1,'1'),
										(2,'2'),
										(3,'3'),
										(4,'4'),
										(5,'5')],'How many days per week'),
	'meals_per_day':fields.selection([(1,'1'),
									(2,'2'),
									(3,'3'),
									(4,'4'),],'No of meals per day'),
	'meal_plan_type':fields.selection([('high carb','High Carb'),
											('low carb','Low Carb'),
											('customize','Customize'),],'Meal Plan Type'),
	'breakfast':fields.boolean('BREAKFAST'),
	'mid':fields.boolean('MID'),
	'evening':fields.boolean('EVENING'),
	'night':fields.boolean('NIGHT'),
	'item_id':fields.many2one('recipies.meal','Relation ID'),

	'wizard_items':fields.many2many('recipies.meal','many_recipies_wizradid','item_id','wizard_rel_id','Items'),
	}

	@api.onchange('current_date')
	def _onchange_date(self):

		if datetime.strptime(self.current_date, '%Y-%m-%d').weekday() == 6 or datetime.strptime(self.current_date, '%Y-%m-%d').weekday()==5:
			pass
		else:
			raise osv.except_osv(_('Warning!'), _('Please select Either sat or sun!!'))


	def change_meal_plan(self,cr,uid,ids,context=None):
		vals=[]
		item=[]
		meals_id=[]
		quantity=calories=carb=fat=protein=price=count=0
		meal_plan_id = context['active_ids']
		data = self.read(cr, uid, ids)[0]
		
		meal_obj = self.pool.get('meal.plans').browse(cr,uid,meal_plan_id)[0]
		carb_type = meal_obj.meal_plan_type
		partner_id = meal_obj.customer.id
		items=data['wizard_items']
		weeks=data['no_of_weeks']
		meal_count=data['meals_per_day']
		# week_count=data['days_per_week']
		current_date = data['current_date']
		data.update({'days_per_week':5,'meal_plan_type':carb_type})

		weekly_data ={'cust':partner_id,'days_per_week':5,'meal_plan_type':carb_type,'no_of_weeks':weeks,'meals_per_day':meal_count,'current_date':data['current_date'],'expiry_date':datetime.strptime(data['current_date'], '%Y-%m-%d')+timedelta(days=25)}

		if len(items)<(meal_count):
			raise Warning(_('Please select atleat '+str(meal_count)+' meals'))
		else:
			pass

		# for i in meal_obj.week1_meal_plan + meal_obj.week2_meal_plan + meal_obj.week3_meal_plan + meal_obj.week4_meal_plan + meal_obj.week5_meal_plan + meal_obj.week6_meal_plan + meal_obj.week7_meal_plan + meal_obj.week8_meal_plan + meal_obj.week9_meal_plan + meal_obj.week10_meal_plan + meal_obj.week11_meal_plan + meal_obj.week12_meal_plan:
			
		# 	i.unlink()

		wk_plan_id = self.pool.get('weekly.plans').search(cr,uid,[('cust','=',partner_id)])
		if wk_plan_id:
			wk_plan =self.pool.get('weekly.plans').browse(cr,uid,wk_plan_id[0])
			wk_plan.write(weekly_data)#writing vals to weekly plan
			for wkk in wk_plan.meals_id1 + wk_plan.meals_id2 + wk_plan.meals_id3 +wk_plan.meals_id4 +wk_plan.meals_id5+wk_plan.meals_id6+wk_plan.meals_id7+wk_plan.meals_id8+wk_plan.meals_id9+wk_plan.meals_id10+wk_plan.meals_id11+wk_plan.meals_id12:
				wkk.unlink()


		for ct in xrange(weeks*5):
			for i in xrange(meal_count):
				print "ctttttttttttt",'item'+str(i+1)
			# print [(0,0, {'items':[(6, 0, items[0])]})]
		self.pool.get('meal.plans').write(cr,uid,meal_plan_id[0],data,context=context)#writing vals to meal plan

		# wk_plan_id = self.pool.get('weekly.plans').search(cr,uid,[('cust','=',partner_id)])
		# wk_plan =self.pool.get('weekly.plans').browse(cr,uid,wk_plan_id[0])
		# wk_plan.write(weekly_data)#writing vals to weekly plan

		# meals = meal_obj.week1_meal_plan.ids + meal_obj.week2_meal_plan.ids + meal_obj.week3_meal_plan.ids + meal_obj.week4_meal_plan.ids + meal_obj.week5_meal_plan.ids + meal_obj.week6_meal_plan.ids + meal_obj.week7_meal_plan.ids + meal_obj.week8_meal_plan.ids + meal_obj.week9_meal_plan.ids + meal_obj.week10_meal_plan.ids + meal_obj.week11_meal_plan.ids + meal_obj.week12_meal_plan.ids
		# meals_id=[]
		# for i in meals:
		# 	obj=self.pool.get('meal.meal').browse(cr,uid,i)
		# 	day = obj.day

		# 	if obj.item1: item += [obj.item1.id]
		# 	if obj.item2: item += [obj.item2.id]
		# 	if obj.item3: item += [obj.item3.id] 
		# 	if obj.item4: item += [obj.item4.id]

		# 	for i in item:
		# 		meals_type = obj.meals_type
		# 		quantity = obj.quantity
		# 		price = obj.price
		# 		carb = obj.carb
		# 		protein = obj.protein
		# 		fat = obj.fat
		# 		calories = obj.calories

		# 	print "0000000000000000000000",item
		# 	vals+=[(0,0, {'day':day,'items':[(6, 0, item)],'meal_plan_type':carb_type,'meals_type':meals_type,'quantity':quantity,'price':price,'carb':carb,'protein':protein,'fat':fat,'calories':calories,})]
		# 	item=[]

		# self.pool.get('meal.plans').write(cr,uid,meal_plan_id[0],{'history_meal':vals},context=context)
		

		# for ml in meal_obj.week1_meal_plan + meal_obj.week2_meal_plan + meal_obj.week3_meal_plan + meal_obj.week4_meal_plan + meal_obj.week5_meal_plan + meal_obj.week6_meal_plan + meal_obj.week7_meal_plan + meal_obj.week8_meal_plan + meal_obj.week9_meal_plan + meal_obj.week10_meal_plan + meal_obj.week11_meal_plan + meal_obj.week12_meal_plan:#unlinking meal plan records
		# 	ml.unlink()

		# for wkk in wk_plan.meals_id1 + wk_plan.meals_id2 + wk_plan.meals_id3 +wk_plan.meals_id4 +wk_plan.meals_id5+wk_plan.meals_id6+wk_plan.meals_id7+wk_plan.meals_id8+wk_plan.meals_id9+wk_plan.meals_id10+wk_plan.meals_id11+wk_plan.meals_id12:
		# 	wkk.unlink()#unlinking weekly plan records

		# for wk in xrange(weeks):
		# 	meals_id=[]
		# 	day=1
		# 	for x in random.sample(items*5, meal_count*week_count):
		# 		count=count+1
				
		# 		if count == meal_count:#to stop creating 5th meal for same day
		# 			itemssss=random.sample(set(items), meal_count)
					
		# 			meals_id = meals_id+[(0,0, {'cust':partner_id,'day':day,'meals_type':'regular','item1':itemssss[0],'item2':itemssss[1],'item3':itemssss[2],'item4':itemssss[3]})]
		# 			day = day + 1
		# 			count = quantity=calories=carb=fat=protein=price=0

		# 	self.pool.get('meal.plans').write(cr,uid,meal_plan_id[0],{'week'+str(wk+1)+'_meal_plan':meals_id,'meal_plan_type':carb_type,'days_per_week':5},context=context)
		return True
Wizard_Meal()

class pause_meal_wizard(osv.TransientModel):

	_name = 'pause.resume.meal'
	_columns = {
	'date': fields.date('Date'),
	}

	def pause_meal_plan(self, cr, uid, ids, context=None):
		input_data = self.read(cr, uid, ids[0])

		if 'type' in context:
			if context['type'] == 'pause': 
				if datetime.strptime(input_data['date'], '%Y-%m-%d').date() == datetime.now().date():
					self.pool.get('meal.plans').write(cr,uid, context['active_ids'],{'state': 'paused','pause_date':input_data['date']})
				else:
					self.pool.get('meal.plans').write(cr,uid, context['active_ids'],{'state': 'active','pause_date':input_data['date']})
			
			elif context['type'] == 'resume': 
				if datetime.strptime(input_data['date'], '%Y-%m-%d').date() == datetime.now().date():
					self.pool.get('meal.plans').write(cr,uid, context['active_ids'],{'state': 'active','resume_date':input_data['date']})
				else:
					self.pool.get('meal.plans').write(cr,uid, context['active_ids'],{'state': 'paused','resume_date':input_data['date']})
			
			else: pass
		return True


class renew_meal_wizard(osv.TransientModel):

	_name = 'renew.meal'
	_columns = {
	'start_date':fields.date('Start Date'),
	'no_of_weeks': fields.selection([(4,'4'),
		(8,'8'),
		(12,'12')],'For how many Weeks ??'),
	}

	def renew_meal_plan(self, cr, uid, ids, context=None):
		input_data = self.read(cr, uid, ids[0])
		# url = ''
		# try:
		# 	plan_obj = self.pool.get('meal.plans').browse(cr, uid, context['active_id'])
		# 	expiry_date = (datetime.today() + timedelta(weeks=input_data['no_of_weeks'])).date()

		# 	data = urllib.urlencode({"state":'active'})
		# 	req = urllib2.Request(url,data=data,headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'})
		# 	response = urllib2.urlopen(req)

		# 	self.pool.get('meal.plans').write(cr,uid, context['active_ids'],{'state': 'active','expiry_date': expiry_date})
		# 	return True

		# except Exception as e:
		# 	raise osv.except_osv(_('Error!'),_('Unable to Renew Meal Plan.'))
		start_date = input_data['start_date']
		meal_plan = context['active_id']
		rec = self.pool.get('meal.plans').browse(cr,uid,meal_plan)
		if rec.no_of_weeks and rec.days_per_week:
			expiry_date = date_by_adding_business_days(datetime.strptime(start_date, '%Y-%m-%d'), ((rec.no_of_weeks*rec.days_per_week)-1))
			if expiry_date: rec.write({'expiry_date':expiry_date,'current_date':start_date})
			return {
			'name':'Renew Meal Plan',
			'view_type':'form',
			'view_mode':'form',
			'target':'current',
			'tag':'reload',
			'type': 'ir.actions.client',
			}

class cancel_meal_wizard(osv.TransientModel):

	_name = 'cancel.meal'
	_columns = {
	'reason': fields.text('Give us the reason??'),
	}

	def cancel_meal_plan(self, cr, uid, ids, context=None):

		# input_data = self.read(cr, uid, ids[0])
		# # print context
		# # print input_data

		# plan_obj = self.pool.get('meal.plans').browse(cr, uid, context['active_id'])
		# # if plan_obj.
		# expiry_date = (datetime.today() + timedelta(weeks=input_data['no_of_weeks'])).date()
		self.pool.get('meal.plans').write(cr,uid, context['active_ids'],{'state': 'cancelled'})

		return True
