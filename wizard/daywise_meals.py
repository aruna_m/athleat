from openerp.osv import osv, fields
import datetime
from datetime import datetime
from collections import Counter
import time
import pytz
from openerp import tools
from openerp.report import report_sxw


class Display_daywise_meals_wizard(osv.osv_memory):

    _name = "daywise.report"

    _columns = {

        'date': fields.date('Date', required="1"),
        'meal_no': fields.selection([(1, 'Morning'),
                                     (2, 'MID'),
                                     (3, 'Evening'),
                                     (4, 'Night')], 'Meal No.'),
    }

    def print_daywise_meal_rep(self, cr, uid, ids, context=None):
        vals = self.read(cr, uid, ids[0], context=context)
        datas = {
            'form': vals,
        }
        return {'type': 'ir.actions.report.xml', 'report_name': 'athleat.display_daywise_meals_list', 'datas': datas, 'context': context}


class details_wizard(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(details_wizard, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'date': self._get_date,
            'recipies': self._get_recipies,
        })

    def _get_date(self, form):
        date = self._get_utc_time_range(form)

    def _get_recipies(self, form):
        data = []
        addons, likes, dislikes = '', '', ''
        low_list = []
        high_list = []
        cust_list = []

        item_no = form['meal_no']

        regular = self.pool.get('meal.meal').search(self.cr, self.uid, [(
            'carb_type', '=', 'high carb'), ('date', '=', form['date'])])
        for i in regular:
            high = {}
            rec = self.pool.get('meal.meal').browse(self.cr, self.uid, i)
            for add in rec.addons.ids:
                addons += self.pool.get('addons.conf').browse(self.cr,self.uid, add).name + ', '
            
            if rec.cust.liked_meals2_ids:
                for like in rec.cust.liked_meals2_ids.ids:
                    likes += self.pool.get('likes.dislikes').browse(self.cr, self.uid, like).name + ', '

            if rec.cust.disliked_meals2_ids:
                for dislike in rec.cust.disliked_meals2_ids.ids:
                    dislikes += self.pool.get('likes.dislikes').browse(self.cr, self.uid, dislike).name + ', ' 

            if item_no == 1:
                high.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                             'no': item_no, 'likes':likes, 'dislikes':dislikes, 'addons': addons, 'item': rec.item1.name})
                high_list += [high]
            elif item_no == 2:
                high.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                             'no': item_no, 'likes':likes,'dislikes':dislikes, 'addons': addons, 'item': rec.item2.name})
                high_list += [high]
            elif item_no == 3:
                high.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                             'no': item_no, 'likes':likes,'dislikes':dislikes, 'addons': addons, 'item': rec.item3.name})
                high_list += [high]
            elif item_no == 4:
                high.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                             'no': item_no, 'likes':likes,'dislikes':dislikes, 'addons': addons, 'item': rec.item4.name})
                high_list += [high]
            else:
                pass

        lowcarb = self.pool.get('meal.meal').search(self.cr, self.uid, [
            ('carb_type', '=', 'low carb'), ('date', '=', form['date'])])
        for i in lowcarb:
            low = {}
            rec = self.pool.get('meal.meal').browse(self.cr, self.uid, i)
            for add in rec.addons.ids:
                addons += self.pool.get('addons.conf').browse(self.cr, self.uid, add).name + ', '
            
            if rec.cust.liked_meals2_ids:
                for like in rec.cust.liked_meals2_ids.ids:
                    likes += self.pool.get('likes.dislikes').browse(self.cr, self.uid, like).name + ', '

            if rec.cust.disliked_meals2_ids:
                for dislike in rec.cust.disliked_meals2_ids.ids:
                    dislikes += self.pool.get('likes.dislikes').browse(self.cr, self.uid, dislike).name + ', ' 

            if item_no == 1:
                low.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                            'no': item_no, 'likes':likes, 'dislikes':dislikes, 'addons': addons, 'item': rec.item1.name})
                low_list += [low]
            elif item_no == 2:
                low.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                            'no': item_no, 'likes':likes, 'dislikes':dislikes, 'addons': addons, 'item': rec.item2.name})
                low_list += [low]
            elif item_no == 3:
                low.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                            'no': item_no, 'likes':likes, 'dislikes':dislikes, 'addons': addons, 'item': rec.item3.name})
                low_list += [low]
            elif item_no == 4:
                low.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                            'no': item_no, 'likes':likes, 'dislikes':dislikes, 'addons': addons, 'item': rec.item4.name})
                low_list += [low]
            else:
                pass

        customize = self.pool.get('meal.meal').search(self.cr, self.uid, [
            ('carb_type', '=', 'customize'), ('date', '=', form['date'])])
        for i in customize:
            cust = {}
            rec = self.pool.get('meal.meal').browse(self.cr, self.uid, i)
            for add in rec.addons.ids:
                addons += self.pool.get('addons.conf').browse(self.cr, self.uid, add).name + ', '
            
            if rec.cust.liked_meals2_ids:
                for like in rec.cust.liked_meals2_ids.ids:
                    likes += self.pool.get('likes.dislikes').browse(self.cr, self.uid, like).name + ', '

            if rec.cust.disliked_meals2_ids:
                for dislike in rec.cust.disliked_meals2_ids.ids:
                    dislikes += self.pool.get('likes.dislikes').browse(self.cr, self.uid, dislike).name + ', ' 

            if item_no == 1:
                cust.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                             'no': item_no, 'likes':likes, 'dislikes':dislikes, 'addons': addons, 'item': rec.item1.name})
                cust_list += [cust]
            elif item_no == 2:
                cust.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                             'no': item_no, 'likes':likes, 'dislikes':dislikes, 'addons': addons, 'item': rec.item2.name})
                cust_list += [cust]
            elif item_no == 3:
                cust.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                             'no': item_no, 'likes':likes, 'dislikes':dislikes, 'addons': addons, 'item': rec.item3.name})
                cust_list += [cust]
            elif item_no == 4:
                cust.update({'cust': rec.cust.name, 'spl_req': rec.spl_req, 'type': rec.carb_type,
                             'no': item_no, 'likes':likes, 'dislikes':dislikes, 'addons': addons, 'item': rec.item4.name})
                cust_list += [cust]
            else:
                pass


        result = {
            'regular': high_list,
            'lowcarb': low_list,
            'customize': cust_list,
        }
        data.append(result)

        if data:
            return data
        else:
            return {}


class report_view_daywise_meal_details(osv.AbstractModel):
    _name = 'report.athleat.display_daywise_meals_list'
    _inherit = 'report.abstract_report'
    _template = 'athleat.display_daywise_meals_list'
    _wrapped_report_class = details_wizard
